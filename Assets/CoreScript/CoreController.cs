﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoreController : MonoBehaviour {

	private ScaleAndRotation scaleAndRotation;
	private SetUpImageTarget setUpImageTarget;
	private GetDeviceRegion getDeviceLanguage;
	private DistanceClass distanceClass;
	private UIController uiController;
	private InputTouch inputTouch;
	private float maxTime = 1;
	private float counter = 0;
	private float numberClick;
	private DistanceClassVuMarker vuDistanceClass;
	public static bool isDoubleClick = false;
	// Use this for initialization
	void Awake () 
	{
		scaleAndRotation = GetComponent<ScaleAndRotation> ();
		setUpImageTarget = GetComponent<SetUpImageTarget> ();
		//microphoneListener = GetComponent<MicrophoneListener> ();
		getDeviceLanguage = GetComponent<GetDeviceRegion> ();
		distanceClass = GetComponent<DistanceClass> ();
		inputTouch = GetComponent<InputTouch> ();
		uiController = GetComponent<UIController> ();
		vuDistanceClass = GetComponent<DistanceClassVuMarker>();
		inputTouch.InputTouchStart ();

	}

	void Start ()
	{
		getDeviceLanguage.GetDeviceLanguage ();
		setUpImageTarget.GetAllImageTargetStart ();

	}
	
	// Update is called once per frame
	void Update ()
	{
		//microphoneListener.MicroPhoneUpdate ();
		scaleAndRotation.RaycastHit ();
		scaleAndRotation.Rotation ();
		scaleAndRotation.OnTouchEnd ();
		distanceClass.DistanaceUpdate ();
		inputTouch.InputTouchUpdate ();
		uiController.UIUpdateFunction ();
		vuDistanceClass.DistanaceVuMarkerUpdate();

		//isDoubleCheck ();
	}


	public void isDoubleCheck ()
	{

		if (counter < maxTime) {
			counter += Time.deltaTime;
			if (Input.GetMouseButtonDown (0)) {
				numberClick++;
				counter = 0;
				if (numberClick >= 2) {
					isDoubleClick = true;
				}
			}
		} else 
		{
			counter = 0;
			isDoubleClick = false;

		}

	}
		

}
