﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoreLanguage : MonoBehaviour {

	private const string Language = "Language";
	public const string DefaultLanguage = "ENGLISH";
	public const string FontBond = "-Bold";
	public const string FontRegular = "-Regular";
	public static Font FontTypeBond;
	public static Font FontTypeRegular;

	public static string LanguagePref
	{
		get {
			if (CCLocalStorageAPI.Contian (Language)) {
		
				return  CCLocalStorageAPI.GET <string> (Language); 
			} else {
				return DefaultLanguage;
			}
		}
		set {
			Debug.Log ("set Language To :" + Language);
			CCLocalStorageAPI.SET<string> (Language, value);
		}
	}



	public static void ChangeLanguage(){
		//	string saveLanguagePath = PersistentPath + LanguageFolder + "/" + languageName;
		//	UILabel[] label = FindObjectsOfType(typeof(UILabel)) as UILabel[]); 
		Debug.Log("ChangeLanguage : " + LanguagePref + " font to :" +FontTypeRegular);
		//UILabel[] label = FindObjectsOfTypeAll(typeof(UILabel))as UILabel[];
		UILabel[] label = Resources.FindObjectsOfTypeAll(typeof(UILabel))as UILabel[];
		Debug.Log (label.Length);
		foreach (UILabel font in label) {
			if (FontTypeRegular != null) {
				font.trueTypeFont = FontTypeRegular;

			}
		}

	}

	public static void SearchForLanguagePack()
	{
		Debug.Log (LoadJson.LocalLanguageJsonDataReference.Count + " " + LanguagePref);
		foreach(LanguageJsonData data in LoadJson.LocalLanguageJsonDataReference)
		{
			if(data.languageName.Equals(LanguagePref)){
				if (data.isLocal) {
					FontTypeBond = Resources.Load (data.fontPackageBoldName) as Font;
					FontTypeRegular = Resources.Load (data.fontPackageRegularName) as Font;
					ChangeLanguage ();
				} else {
					ChangeLanguage ();
				}
				break;
			}

		}
	}

}
