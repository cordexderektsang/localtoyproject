﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoreNguiButtonAddAndRemove : MonoBehaviour {

	public static void AddParameterFunction(MonoBehaviour Script , object [] Parameter , UIButton button , string methodName)
	{
		if (Script != null && button != null && methodName != null) {
			EventDelegate btnFunctionToBeAdd = new EventDelegate (Script, methodName); // add a function from a script
			if (Parameter != null) {//if there are parameter
				for (int i = 0; i < Parameter.Length; i++) {
					btnFunctionToBeAdd.parameters [i].value = Parameter[i];// add all parameter
				}
			} 
			if (button.onClick.Count == 0) {
				EventDelegate.Set (button.onClick, btnFunctionToBeAdd);//set the first script function
			} else {
				EventDelegate.Add (button.onClick, btnFunctionToBeAdd);	// add another script function 
			}
		}
	}

	public static void RemoveParameterFunction(MonoBehaviour Script, object[] Parameter, UIButton button, string methodName)
	{ 
		EventDelegate btnFunctionToBeRemove = new EventDelegate(Script, methodName); // remove a function from a script
		if (Parameter != null)
		{
			//if there are parameter
			for (int i = 0; i<Parameter.Length; i++)
			{
				btnFunctionToBeRemove.parameters[i].value = Parameter[i]; // add all parameter
			}
		}
		EventDelegate.Remove (button.onClick, btnFunctionToBeRemove); // remove the selected function
	}
	//AbstractLoadAssetDataAndCreateUIButton.AddParameterFunction(this, null, uiButton, "SetupDownloadList");
	//AbstractLoadAssetDataAndCreateUIButton.AddParameterFunction(networkCheckingController, new object[] { ErrorPopup }, uiButton, "DisableNetworkPrompt");
}
