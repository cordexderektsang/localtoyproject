﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using System.IO;
using CCARPlatformFramework.CCConstants;
using CCARPlatformFramework.CCDataHandling;
using CCARPlatformFramework.CCTools;


public class DynamicLoadMarker : MonoBehaviour {



	public SetUpImageTarget setUpImageTarget;

	//public GameObject targetDisplayObject;
	private string markerDataSet = "VUmarkerTest.xml";
	private string fullPath = "";
	// Use this for initialization

	public void Start ()
	{
		loadXmlAndDat ();
	


	}
	public void loadXmlAndDat(/*string markerName*/)
	{

		//VuforiaBehaviour vb = GameObject.FindObjectOfType<VuforiaBehaviour>();
		//vb.RegisterVuforiaStartedCallback (() => LoadDataSet(markerName));
		VuforiaARController vuforia = VuforiaARController.Instance;
		foreach (MarkerJsonData data in LoadJson.LocalMarkerJsonDataReference) {
			string filePath = data.MarkerDatPath +  data.markerXmlName ;
			vuforia.RegisterVuforiaStartedCallback (() => LoadDataSet (filePath));

		}
		LoadObjectData ();	
	}

	/// <summary>
	/// generate all image object
	/// </summary>
	public void LoadDataSet(string fileName)
	{
		//fullPath = Path.Combine (Application.persistentDataPath ,markerDataSet);
		//Debug.Log (fullPath);
		//Debug.Log("step 5 load data set with following fileName : " + fileName);
		ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();

		//IEnumerable<DataSet> loadedDataSets = objectTracker.GetDataSets();
		DataSet dataSet = objectTracker.CreateDataSet();
		bool isFileExist = File.Exists (fileName);
		Debug.Log ("Do File Path Exist : " + isFileExist);
		Debug.Log (fileName);
		if (dataSet.Load (fileName, VuforiaUnity.StorageType.STORAGE_ABSOLUTE)) {
			Debug.Log ("Loading");
			objectTracker.Stop ();
			if (!objectTracker.ActivateDataSet (dataSet)) {
				// Note: ImageTracker cannot have more than 100 total targets activated
				Debug.Log ("<color=yellow>Failed to Activate DataSet: " + fileName + "</color>");
			}

			if (!objectTracker.Start ()) {
				Debug.Log ("<color=yellow>Tracker Failed to Start.</color>");
			}

		} else {

			Debug.Log ("fail");
		}

		/*foreach (DataSet loadedDataSet in loadedDataSets)
		{
			Debug.Log("testing the path: " + loadedDataSet.Path);

		}*/

		/*IEnumerable<TrackableBehaviour> tbs = TrackerManager.Instance.GetStateManager().GetTrackableBehaviours();

		foreach (TrackableBehaviour tb in tbs)
		{

				//if (data.trackableName.Equals (tb.TrackableName)) 
				//{
				// change generic name to include trackable name
				//tb.gameObject.name = tb.TrackableName;

				// add additional script components for trackable
				//tb.gameObject.AddComponent<CoreCustomTrackableEventHandler>();
				//GameObject target = Instantiate (targetDisplayObject);
			//Debug.Log ("lalal");
			//	target.transform.parent = tb.gameObject.transform;

				//dataPath.downLoadimageTargetJsonData (tb.TrackableName);										


		}*/
	}



	public void LoadObjectData ()
	{
		Debug.Log ("==================" + LoadJson.LocalObjectJsonDataReference.Count);
		foreach (ObjectJsonData data in LoadJson.LocalObjectJsonDataReference)
		{
			Debug.Log ("==================" + setUpImageTarget.objectForTracker.Count);
			foreach (ImageObjectForEachTracker ImageData in  setUpImageTarget.objectForTracker)
			{
	
				if (data.associatedMarkerName.Equals (ImageData.markerTrackableName)) {
					Debug.Log ("======" +ImageData.markerTrackableName  + " ===" + data.associatedMarkerName );
					ImageData.gameObjectPosition = data.ObjectPosition;
					ImageData.gameObjectRotation = data.ObjectRotation;
					ImageData.gameObjectScale = data.ObjectScale;
					GameObject markerObject = LoadAssetBundle (data.ObjectPath,data.ObjectFileName +GlobalStaticVariable.AssetBundle_EXT);
					ImageData.markerGameObject = markerObject;
				}

			}
		}
		setUpImageTarget.gameObject.SetActive (true);

	}

	public static GameObject LoadAssetBundle (string bundlePath, string bundleFile )
	{

		AssetBundle currentBuddle = CCAssetBundleManager.GetAssetBundle(bundlePath, bundleFile);
		string[] objectName = CCAssetBundleManager.ReturnAllAssetNameInBundle (currentBuddle);
		GameObject augmentationObject = (CCAssetBundleManager.GetGameObjectFromAssetBundle(currentBuddle, objectName[0]));
		return augmentationObject;

	}

	public void LoadEverythingInBundle ()
	{
		

	}
}
