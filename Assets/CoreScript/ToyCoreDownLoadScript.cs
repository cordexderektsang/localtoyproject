﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CCARPlatformFramework.CCConstants;
using CCARPlatformFramework.CCDataHandling;
using CCARPlatformFramework.CCTools;
using System.IO;

public class ToyCoreDownLoadScript : MonoBehaviour {


	public const string MainFolder = "4M";
	public const string JsonFolder = "/Jsons";
	public const string LanguageFolder = "/Language";
	public const string ObjectsFolder = "/Objects";
	public const string PictureFolder = "/Pictures";
	public const string SceneFolder = "/Scenes";
	public const string AudioFolder = "/Audio";
	public const string XMlFolder = "/Xml";
	public const string UIFolder = "/UI";
	public const string Android = "/Android";
	public const string IOS = "/IOS";
	public string PersistentPath = "";

	public string downLoadURL ="";
	public bool isNetworkError = false;
	public string path = "";
	public string fileName = "";
	public string bundlePath = "";
	public string bundleFile = "";
	public bool isLocal = true;
	public ScaleScroller ScaleScroller;
	public LoadingBar loadingBar;
	private LoadJson loadJson;
	private bool isJsonExist = false;
	private bool isFinsishDownloadTask = true;

	//public delegate void DisconnectFunction (string downLoadURL , string path , string fileName ,FileDownloadType typeOfDownload , int index);
	//public DisconnectFunction downloadFunction;

	public enum FileDownloadType {MarkerAndObjectJson, MenuJson, LanguageJson ,MenuPng, LanguagePng,LanguageBundle, SceneBundle, ObjectBundle, MarkerBundle ,Nothing};
	private FileDownloadType typeOfDownload = FileDownloadType.Nothing;


	public void Awake ()
	{
		#if UNITY_IOS

		downLoadURL += MainFolder +IOS;

		#elif UNITY_ANDROID

		downLoadURL+= MainFolder +Android;
		#endif
		PersistentPath = Application.persistentDataPath;
		CreateDictionary ();

		if (isLocal) {
			ScaleScroller.enabled = true;
		} else {
			
			loadJson = GetComponent<LoadJson> ();
			isJsonExist = loadJson.CheckAndStoreLocalJson ();
			DownloadLanguageJson ();
			DownloadMenuItemJson ();
		
		}
	}




	public void DownloadLanguageJson()
	{
		//Download Language Json
		string languagePath = downLoadURL + JsonFolder + "/" + LoadJson.languageList ;
		//string languagePath = downLoadURL + MainFolder + JsonFolder + "/" + "lalal" ;
		string saveLanguagePath = PersistentPath + JsonFolder + "/";
		DownloadFunction (languagePath, saveLanguagePath, LoadJson.languageList, FileDownloadType.LanguageJson);

	}

	public void DownloadMenuItemJson()
	{

		//Download Menu Item Json
		string menuItemsPath = downLoadURL  + JsonFolder + "/" + LoadJson.listOfSceneAndUI;
		string saveMenuItemsPath = PersistentPath + JsonFolder + "/";
		DownloadFunction (menuItemsPath,saveMenuItemsPath, LoadJson.listOfSceneAndUI,FileDownloadType.MenuJson);

	}


	public void DownloadPicturePNG (){

		StartCoroutine (DownLoadPictureSequence());

	}

	public IEnumerator DownLoadPictureSequence ()
	{
		string savePicturePath = PersistentPath + PictureFolder + "/";
		int indexForLoopsForMenu= 0;
		int indexForLoopsForLanguage= 0;
		while (true) {
			yield return new  WaitForSeconds (0.01f);
			if (!isFinsishDownloadTask) {
				yield return null;
			} else if (isFinsishDownloadTask) {

				// Menu Image
				if (indexForLoopsForMenu < LoadJson.LocalMenuJsonDataReference.Count) {
					Debug.Log ("======== step 2 === Download  Menu Png =============");
					/*string menuLocalPicturePath = PersistentPath + PictureFolder + "/" + LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].pictureName + GlobalStaticVariable.PNG;
					if (LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].isDownload || !IsFileckmExist(menuLocalPicturePath+GlobalStaticVariable.CHECKSUM_EXT)) {
						Debug.Log ("Download For menu pictrue For " + LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].pictureName );
						string menuPicturePath = downLoadURL + MainFolder + PictureFolder + "/" + LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].pictureName;
						DownloadFunction (menuPicturePath, savePicturePath, LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].pictureName + GlobalStaticVariable.PNG, FileDownloadType.MenuPng, indexForLoopsForMenu);
	
					} else {
						Debug.Log ("Load local For menu pictrue"  + LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].pictureName );
						string saveMenuPath = PersistentPath + PictureFolder + "/";
						loadJson.SwitchCaseForLoadImage (saveMenuPath, LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].pictureName + GlobalStaticVariable.PNG, FileDownloadType.MenuPng, indexForLoopsForMenu);

					}*/
					DownloadMenuPNG (indexForLoopsForMenu , savePicturePath);
					indexForLoopsForMenu++;

				} else
				// Language Image
				if (indexForLoopsForLanguage < LoadJson.LocalLanguageJsonDataReference.Count) {
						Debug.Log ("======== step 3 === Download  Language Png =============");
					/*string languageLocalPicturePath = PersistentPath + PictureFolder + "/" + LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].languageName + GlobalStaticVariable.PNG;
					if (LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].isLocal) {
						// do nothing let it load from resource
						} else if (LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].isDownload || !IsFileckmExist(languageLocalPicturePath+GlobalStaticVariable.CHECKSUM_EXT)) {
							Debug.Log ("Download For Language pictrue"  + LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].languageName );
						string languagePicturePath = downLoadURL + MainFolder + PictureFolder + "/" + LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].languageName;
						DownloadFunction (languagePicturePath, savePicturePath, LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].languageName + GlobalStaticVariable.PNG, FileDownloadType.LanguagePng, indexForLoopsForLanguage);
					} else if (!LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].isLocal) {
							Debug.Log ("Load local For Language pictrue" + LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].languageName);
						string saveLanguagePath = PersistentPath + PictureFolder + "/";
						loadJson.SwitchCaseForLoadImage (saveLanguagePath, LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].languageName + GlobalStaticVariable.PNG, FileDownloadType.LanguagePng, indexForLoopsForLanguage);
					}*/
					DownloadLanguagePNG (indexForLoopsForLanguage,savePicturePath);
					indexForLoopsForLanguage++;

				}

				if (indexForLoopsForMenu == LoadJson.LocalMenuJsonDataReference.Count - 1 && indexForLoopsForLanguage == LoadJson.LocalLanguageJsonDataReference.Count - 1) {
					Debug.Log ("======== step 4 === Finish Download  Menu Png =============");
					break;
				}


			}
		}

	} 

	public void DownloadMenuPNG (int indexForLoopsForMenu, string savePicturePath)
	{

		string menuLocalPicturePath = PersistentPath + PictureFolder + "/" + LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].pictureName + GlobalStaticVariable.PNG;
		if (LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].isDownload || !IsFileckmExist(menuLocalPicturePath+GlobalStaticVariable.CHECKSUM_EXT)) {
			Debug.Log ("Download For menu pictrue For " + LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].pictureName );
			string menuPicturePath = downLoadURL  + PictureFolder + "/" + LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].pictureName;
			DownloadFunction (menuPicturePath, savePicturePath, LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].pictureName + GlobalStaticVariable.PNG, FileDownloadType.MenuPng, indexForLoopsForMenu);

		} else {
			Debug.Log ("Load local For menu pictrue"  + LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].pictureName );
			string saveMenuPath = PersistentPath + PictureFolder + "/";
			loadJson.SwitchCaseForLoadImage (saveMenuPath, LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].pictureName + GlobalStaticVariable.PNG, FileDownloadType.MenuPng, indexForLoopsForMenu);

		}

	}

	public void DownloadLanguagePNG(int indexForLoopsForLanguage, string savePicturePath)
	{

		string languageLocalPicturePath = PersistentPath + PictureFolder + "/" + LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].languageName + GlobalStaticVariable.PNG;
		if (LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].isLocal) {
			// do nothing let it load from resource
		} else if (LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].isDownload || !IsFileckmExist(languageLocalPicturePath+GlobalStaticVariable.CHECKSUM_EXT)) {
			Debug.Log ("Download For Language pictrue"  + LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].languageName );
			string languagePicturePath = downLoadURL  + PictureFolder + "/" + LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].languageName;
			DownloadFunction (languagePicturePath, savePicturePath, LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].languageName + GlobalStaticVariable.PNG, FileDownloadType.LanguagePng, indexForLoopsForLanguage);
		} else if (!LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].isLocal) {
			Debug.Log ("Load local For Language pictrue" + LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].languageName);
			string saveLanguagePath = PersistentPath + PictureFolder + "/";
			loadJson.SwitchCaseForLoadImage (saveLanguagePath, LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].languageName + GlobalStaticVariable.PNG, FileDownloadType.LanguagePng, indexForLoopsForLanguage);
		}



	}


	public void DownloadLanguagePack (string languagePack)
	{

		string saveLanguagePath = PersistentPath + LanguageFolder ;
		string downloadLanguagePath = downLoadURL  + LanguageFolder + "/"  + languagePack.ToLower() +GlobalStaticVariable.AssetBundle_EXT;
		Debug.Log ("=========== DownLoad Language Language ===== :  " + languagePack);
		DownloadFunction (downloadLanguagePath,saveLanguagePath,languagePack.ToLower()+GlobalStaticVariable.AssetBundle_EXT, FileDownloadType.LanguageBundle);

	}

	public void DownloadObjectAndMarkerJson (string FileName)
	{

		string savebjectAndMarkerPath = PersistentPath + JsonFolder ;
		string downloadLanguagePath = downLoadURL  + JsonFolder + "/"  + FileName;
		Debug.Log ("====== Download Marker and Object Json ===== " + downloadLanguagePath + " Saved To : " + savebjectAndMarkerPath);
		DownloadFunction (downloadLanguagePath,savebjectAndMarkerPath,FileName, FileDownloadType.MarkerAndObjectJson);

	}


	public void DownloadMarkerAndObject ()
	{

		StartCoroutine (DownLoadMarkerAndObjectSequence());

	}
		


	public IEnumerator DownLoadMarkerAndObjectSequence ()
	{
		string saveXmlPath = PersistentPath + XMlFolder + "/";
		string saveObjectPath = PersistentPath + ObjectsFolder + "/";
		string saveScenePath = PersistentPath + SceneFolder + "/";
		string saveAudioPath = PersistentPath + AudioFolder + "/";
		string saveUIPath = PersistentPath + UIFolder + "/";
		int indexForLoopsForObject= 0;
		int indexForLoopsForMarker= 0;
		bool startSceneDownload = false;
		bool startAudioDownload = false;
		bool startUIDownload = false;
		bool downloadXml = false;
		bool downloadComplete = false;

		while (true) {
			yield return new  WaitForSeconds (0.01f);
			if (!isFinsishDownloadTask) {
				yield return null;
			} else if (isFinsishDownloadTask) {

				if (indexForLoopsForObject < LoadJson.LocalObjectJsonDataReference.Count) {
					Debug.Log ("==========Step 5 For Download Scene Sequenece - Objects ==========");
					DownloadObject (saveObjectPath, indexForLoopsForObject);
					indexForLoopsForObject++;

				} else if (indexForLoopsForMarker < LoadJson.LocalMarkerJsonDataReference.Count) {
					Debug.Log ("==========Step 6 For Download Scene Sequenece - Markers ==========");
					DownloadMarker (saveXmlPath, downloadXml, indexForLoopsForMarker);
					indexForLoopsForMarker++;

				} else {

					if (!startSceneDownload) {
						startSceneDownload = DownloadScene (saveScenePath, startSceneDownload);
					}
					if (!startAudioDownload) {
						startAudioDownload = DownloadAudio (saveAudioPath, startSceneDownload, startAudioDownload);
					}
					if (!startUIDownload) {
						startUIDownload = DownloadUI (saveUIPath, startAudioDownload, startUIDownload);
					}
					if (!downloadComplete) {
						downloadComplete = DownloadComplete (startUIDownload, downloadComplete);
					}

				} 
			}
		}

	}

	public bool DownloadComplete (bool startUIDownload , bool downloadComplete){

		if(startUIDownload && !downloadComplete && isFinsishDownloadTask){

			Debug.Log ("==========Step 10 For Download Scene Sequenece - Complete ==========");
			GlobalStaticVariable.LoadUiBundle ();
			GlobalStaticVariable.finishDownload = true;
			return true;
		}
		return false;

	}

	public bool DownloadUI (string saveUIPath ,bool startAudioDownload , bool startUIDownload )
	{

		if (startAudioDownload && isFinsishDownloadTask && !startUIDownload) {
			Debug.Log ("==========Step 9 For Download Scene Sequenece - UI ==========");
			string audioLocalUIPathDat = PersistentPath + UIFolder + "/" + LoadJson.LocalUIJsonDataReference [0].UIName;
			if (LoadJson.LocalUIJsonDataReference [0].isDownload || !IsFileckmExist (audioLocalUIPathDat + GlobalStaticVariable.AssetBundle_EXT + GlobalStaticVariable.CHECKSUM_EXT)) {
				Debug.Log ("Download For UI  For " + LoadJson.LocalUIJsonDataReference [0].UIName);
				string datDownloadPath = downLoadURL  + UIFolder + "/" + LoadJson.LocalUIJsonDataReference [0].UIName.ToLower () + GlobalStaticVariable.AssetBundle_EXT;
				LoadJson.LocalUIJsonDataReference [0].UIPath = saveUIPath;
				LoadJson.LocalUIJsonDataReference [0].UIFileName = LoadJson.LocalUIJsonDataReference [0].UIName.ToLower ()+ GlobalStaticVariable.AssetBundle_EXT;
				DownloadFunction (datDownloadPath, saveUIPath, LoadJson.LocalUIJsonDataReference [0].UIName.ToLower () + GlobalStaticVariable.AssetBundle_EXT, FileDownloadType.SceneBundle, 0);

			} else {
				Debug.Log ("Local For Ui  For " + LoadJson.LocalUIJsonDataReference [0].UIName);
				LoadJson.LocalUIJsonDataReference [0].UIPath = saveUIPath;
				LoadJson.LocalUIJsonDataReference [0].UIFileName = LoadJson.LocalUIJsonDataReference [0].UIName.ToLower () + GlobalStaticVariable.AssetBundle_EXT;
			}


			return true;

		}
		return false;
	}

	public bool DownloadAudio (string saveAudioPath ,bool startSceneDownload , bool startAudioDownload )
	{

		if (startSceneDownload && isFinsishDownloadTask && !startAudioDownload) {
			Debug.Log ("==========Step 8 For Download Scene Sequenece - Audio ==========");
			string audioLocalPicturePathDat = PersistentPath + AudioFolder + "/" + LoadJson.LocalAudioJsonDataReference [0].AudioName;
			if (LoadJson.LocalAudioJsonDataReference [0].isDownload || !IsFileckmExist (audioLocalPicturePathDat + GlobalStaticVariable.AssetBundle_EXT + GlobalStaticVariable.CHECKSUM_EXT)) {
				Debug.Log ("Download For Audio  For " + LoadJson.LocalAudioJsonDataReference [0].AudioName);
				string datDownloadPath = downLoadURL  + AudioFolder + "/" + LoadJson.LocalAudioJsonDataReference [0].AudioName.ToLower () + GlobalStaticVariable.AssetBundle_EXT;
				LoadJson.LocalAudioJsonDataReference [0].AudioPath = saveAudioPath;
				LoadJson.LocalAudioJsonDataReference [0].AudioName = LoadJson.LocalAudioJsonDataReference [0].AudioName.ToLower () + GlobalStaticVariable.AssetBundle_EXT;
				DownloadFunction (datDownloadPath, saveAudioPath, LoadJson.LocalAudioJsonDataReference [0].AudioName.ToLower () + GlobalStaticVariable.AssetBundle_EXT, FileDownloadType.SceneBundle, 0);

			} else {
				Debug.Log ("Local For Audio  For " + LoadJson.LocalAudioJsonDataReference [0].AudioName);
				LoadJson.LocalAudioJsonDataReference [0].AudioPath = saveAudioPath;
				LoadJson.LocalAudioJsonDataReference [0].AudioName = LoadJson.LocalAudioJsonDataReference [0].AudioName.ToLower ()+ GlobalStaticVariable.AssetBundle_EXT;
			}


			return true;
		}
		return false;
	}


	public bool DownloadScene (string saveScenePath  ,bool startSceneDownload)
	{

		if (!string.IsNullOrEmpty (GlobalStaticVariable.CurrentOnClickSceneName) && isFinsishDownloadTask && !startSceneDownload) {
			Debug.Log ("==========Step 7 For Download Scene Sequenece - Scene ==========");
			string localScenePathDat = PersistentPath + SceneFolder + "/" + LoadJson.LocalSceneJsonDataReference [0].SceneName;
			if (LoadJson.LocalSceneJsonDataReference [0].isDownload || !IsFileckmExist (localScenePathDat + GlobalStaticVariable.AssetBundle_EXT + GlobalStaticVariable.CHECKSUM_EXT)) {
				Debug.Log ("Download For Scene  For " + GlobalStaticVariable.CurrentOnClickSceneName);
				string datDownloadPath = downLoadURL  + SceneFolder + "/" + GlobalStaticVariable.CurrentOnClickSceneName.ToLower () + GlobalStaticVariable.AssetBundle_EXT;
				LoadJson.LocalSceneJsonDataReference [0].ScenePath = saveScenePath;
				LoadJson.LocalSceneJsonDataReference [0].SceneFileName = GlobalStaticVariable.CurrentOnClickSceneName.ToLower () +  GlobalStaticVariable.AssetBundle_EXT;
				DownloadFunction (datDownloadPath, saveScenePath, GlobalStaticVariable.CurrentOnClickSceneName.ToLower () + GlobalStaticVariable.AssetBundle_EXT, FileDownloadType.SceneBundle, 0);
			} else {
				LoadJson.LocalSceneJsonDataReference [0].ScenePath = saveScenePath;
				LoadJson.LocalSceneJsonDataReference [0].SceneFileName = GlobalStaticVariable.CurrentOnClickSceneName.ToLower ()+ GlobalStaticVariable.AssetBundle_EXT;
				Debug.Log ("Local For Scene  For " + GlobalStaticVariable.CurrentOnClickSceneName);
			}

			return true;
		}

		return false;

	}

	public void DownloadMarker (string saveXmlPath  ,bool downloadXml , int indexForLoopsForMarker)
	{
		if (isFinsishDownloadTask && !downloadXml) {
			string markerLocalPicturePathXml = PersistentPath + XMlFolder + "/" + LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerXmlName;
			if (LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].isDownload || !IsFileckmExist (markerLocalPicturePathXml + GlobalStaticVariable.CHECKSUM_EXT)) {
				Debug.Log ("Download For  xml Marker  For " + LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerXmlName);
				string xmLDownloadPath = downLoadURL  + XMlFolder + "/" + LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerXmlName;
				LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerXmlPath = saveXmlPath;
				LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerXmlFileName = LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerXmlName;
				DownloadFunction (xmLDownloadPath, saveXmlPath, LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerXmlFileName, FileDownloadType.MarkerBundle, indexForLoopsForMarker);
				downloadXml = true;

			} else {

				Debug.Log ("local For marker xml ");
				LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerXmlPath = saveXmlPath;
				LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerXmlFileName = LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerXmlName;
				downloadXml = true;
			}
		}
		if (isFinsishDownloadTask && downloadXml) {
			string markerLocalPicturePathDat = PersistentPath + XMlFolder + "/" + LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerDatName;

			if (LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].isDownload || !IsFileckmExist (markerLocalPicturePathDat + GlobalStaticVariable.CHECKSUM_EXT)) {
				Debug.Log ("Download For dat  Marker  For " + LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerDatName);
				string datDownloadPath = downLoadURL  + XMlFolder + "/" + LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerDatName;
				LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerDatPath = saveXmlPath;
				LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerDatFileName = LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerDatName;
				DownloadFunction (datDownloadPath, saveXmlPath, LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerDatFileName, FileDownloadType.MarkerBundle, indexForLoopsForMarker);
				downloadXml = false;
				indexForLoopsForMarker++;

			} else {
				Debug.Log ("local For marker Dat");
				LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerDatPath = saveXmlPath;
				LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerDatFileName = LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerDatName;
				downloadXml = false;
				indexForLoopsForMarker++;
			}
		}

	}

	public void DownloadObject (string saveObjectPath , int indexForLoopsForObject)
	{
		string objectLocalPicturePath = PersistentPath + ObjectsFolder + "/" + LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].objectName + GlobalStaticVariable.AssetBundle_EXT;
		if (LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].isDownload || !IsFileckmExist (objectLocalPicturePath + GlobalStaticVariable.CHECKSUM_EXT)) {
			Debug.Log ("Download For Object For " + LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].objectName);
			string objectDownloadPath = downLoadURL  + ObjectsFolder + "/" + LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].objectName + GlobalStaticVariable.AssetBundle_EXT;
			LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].ObjectPath = saveObjectPath;
			LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].ObjectFileName = LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].objectName;
			DownloadFunction (objectDownloadPath, saveObjectPath, LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].objectName + GlobalStaticVariable.AssetBundle_EXT, FileDownloadType.ObjectBundle, indexForLoopsForObject);

		} else {
			Debug.Log ("local For Object For " + LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].objectName);
			LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].ObjectPath = saveObjectPath;
			LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].ObjectFileName = LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].objectName;

		}

	}

	public void CreateDictionary ()
	{

		string jsonFolder = PersistentPath + JsonFolder;
		string languageFolder = PersistentPath + LanguageFolder;
		string objectsFolder = PersistentPath + ObjectsFolder;
		string picutrefolder = PersistentPath + PictureFolder;
		string sceneFolder = PersistentPath + SceneFolder;
		string xmlFolder = PersistentPath + XMlFolder;
		string audioFolder = PersistentPath + AudioFolder;
		string uiFolder = PersistentPath + UIFolder;
		if(!Directory.Exists(jsonFolder)){
			Directory.CreateDirectory (jsonFolder);
		}
		if(!Directory.Exists(languageFolder)){
			Directory.CreateDirectory (languageFolder);
		}
		if(!Directory.Exists(objectsFolder)){
			Directory.CreateDirectory (objectsFolder);
		}
		if(!Directory.Exists(picutrefolder)){
			Directory.CreateDirectory (picutrefolder);
		}
		if(!Directory.Exists(sceneFolder)){
			Directory.CreateDirectory (sceneFolder);
		}
		if(!Directory.Exists(xmlFolder)){
			Directory.CreateDirectory (xmlFolder);
		}
		if(!Directory.Exists(audioFolder)){
			Directory.CreateDirectory (audioFolder);
		}
		if(!Directory.Exists(UIFolder)){
			Directory.CreateDirectory (uiFolder);
		}


	}
	/* Download Flows
	 * step 1
	 * 1.check internet
	 * 2. get json for number of scenes , each scene name ,number of languages , each pictures
	 * 2.1 read json
	 * 2.2 set up nubmer of language for selection
	 * 2.3 add scene name to target button
	 * 2.4 download pictures.
	 * 2.5 during scroll disable
	 * 3 Download picture for menu display
	 * 4 if fail cycle from step 1 start
	 * step 2
	 * 5 check internet
	 * 5.5 check for is it trial or full
	 * 6.get json for download list - markers , unicorn , element then scene
	 * 6.1 read json
	 * 6.2 download markers xml and dat
	 * 6.3 download all unicorn
	 * 6.4 download all elements
	 * 6.5 download one scene
	 * 7 check sum for each bundle
	 * 8. if fail cycle from step 2 start
	 * 9. load Game.
	 * 10. fail carry on if can
	 */

	/* To do List
		Test 8/9 languages
		start write download flow
		start write/read json
		disconnect and reconnect flow
		progess bar and MB
		Xcode test put in album function
		photo UI for preview and share
		language option , audio option , scene option
		each audio source for each unicorn , background ,and micro
		online marker loading
		setup address for bundles , scenes upload and download test
		Get CMS notice for product information

	*/

	public void DownloadFunction (string downLoadURL , string path , string fileName ,FileDownloadType typeOfDownload , int index = 0)
	{
		Debug.Log (downLoadURL);
		CCLoadDataManager.downloadFunction += loadingBar.wwwProgress;
		isFinsishDownloadTask = false;
		CCDownloadController downloadController = CCDownloadController.Instance;
		downloadController.DownloadSingleFile (downLoadURL, CCLoadDataManager.DownloadProgress, (WWW www) => {
			if(string.IsNullOrEmpty(www.error))
			{
				Debug.Log( path +" : " +  fileName);
				CCWriteDataManager.WriteToPath (path, fileName, www.bytes);

				SwitchCaseForJson(www.text,typeOfDownload);
				loadJson.SwitchCaseForLoadImage(path,fileName,typeOfDownload,index);
				LoadFontBundle(typeOfDownload,fileName,path,fileName);
				www.Dispose ();
				CCLoadDataManager.downloadFunction -= loadingBar.wwwProgress;
				isNetworkError = false;
				CCWriteDataManager.WriteToPath(path, fileName +GlobalStaticVariable.CHECKSUM_EXT, "");
				isFinsishDownloadTask = true;

			}
			else
			{
				Debug.Log("error" + fileName + " : " + typeOfDownload.ToString());
				NetworkCoreCheckHandler.isNetworkError = true;
				
				CCLoadDataManager.downloadFunction -= loadingBar.wwwProgress;
				DownloadFunction(downLoadURL,path,fileName,typeOfDownload,index);
			}



		},
			() =>
			{
				Debug.Log("fail Download" + fileName + " : " + typeOfDownload.ToString());
				isNetworkError = true;
				CCLoadDataManager.downloadFunction -= loadingBar.wwwProgress;
				DownloadFunction(downLoadURL,path,fileName,typeOfDownload,index);
				NetworkCoreCheckHandler.isNetworkError = true;

			});

	}



	public void SwitchCaseForJson (string JsonText , FileDownloadType typeOfDownload)
	{
		switch(typeOfDownload){
		case FileDownloadType.LanguageJson:
			loadJson.ReadOnlineJsonForLanguage (JsonText);
			break;
		case FileDownloadType.MenuJson:
			loadJson.ReadOnlineJsonForMenu (JsonText);
			break;
		case FileDownloadType.MarkerAndObjectJson:
			loadJson.ReadOnlineJsonForMarkerAndObject (JsonText);
			break;
		}

	}



	public void LoadFontBundle (FileDownloadType typeOfDownload ,string LanguageName , string bundlePath, string bundleFile)
	{
		switch (typeOfDownload) {
		case FileDownloadType.LanguageBundle:
			AssetBundle currentBuddle = CCAssetBundleManager.GetAssetBundle (bundlePath, bundleFile);
			string language = LanguageName.Replace (GlobalStaticVariable.AssetBundle_EXT, "");
			Debug.Log (language);
			CoreLanguage.FontTypeBond = CCAssetBundleManager.GetFontFromAssetBundle (currentBuddle, language+CoreLanguage.FontBond );
			CoreLanguage.FontTypeRegular = CCAssetBundleManager.GetFontFromAssetBundle (currentBuddle, language+CoreLanguage.FontRegular );
			//Debug.Log (LanguageController.FontType);
			CoreLanguage.SearchForLanguagePack ();
			break;
		}

	}

	public bool IsFileckmExist (string path)
	{
		return File.Exists (path);
	}


	public void LoadAssetBundle ()
	{

		AssetBundle currentBuddle = CCAssetBundleManager.GetAssetBundle(bundlePath, bundleFile);
		string objectName = "";
		GameObject augmentationObject = (CCAssetBundleManager.GetGameObjectFromAssetBundle(currentBuddle, objectName));

	}


	void OnDestroy() 
	{
		
		CCLoadDataManager.downloadFunction = null;
	}

	/*
	 * json for number of scene
	 * json for number of languages
	 * download target scene
	 * download list of bundles
	 * set up marker
	 * 
	*/

}
