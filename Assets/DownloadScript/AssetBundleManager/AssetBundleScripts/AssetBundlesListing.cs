﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

public class AssetBundlesListing : EditorWindow
{
    private static AssetBundlesProperties AssetBundlesProperties;

    private Vector2 scrollPos;
    private int assetCount = -1;

    public static void Init(AssetBundlesProperties assetBundlesProperties)
    {
        Debug.Log("Opening");

        AssetBundlesProperties = assetBundlesProperties;

        ShowWindow();
    }

    public static void ShowWindow()
    {
        EditorWindow window = CreateInstance<AssetBundlesListing>();
        window.titleContent.text = "List of Assets";
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("List of Assets for bundle: " + AssetBundlesProperties.assetBundleName, EditorStyles.boldLabel);

        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

        foreach(string asset in AssetBundlesProperties.assets)
        {
            EditorGUILayout.SelectableLabel(asset, EditorStyles.textField, GUILayout.Height(EditorGUIUtility.singleLineHeight));
        }

        if (GUILayout.Button("Close", GUILayout.MaxWidth(80.0f)))
        {
            Close();
        }

        EditorGUILayout.EndScrollView();
    }
}
#endif