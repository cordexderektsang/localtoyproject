﻿using CCARPlatformFramework.CCInterfaces;
using CCARPlatformFramework.CCObjects;
using System.Collections.Generic;
using UnityEngine;

namespace CCARPlatformFramework
{
    public class CCARPlatformSkeleton : MonoBehaviour
    {
        #region STATIC_AND_ENUM

        public enum PERFORM_ACTION_DURING {
            ON_TRACKING_FOUND, ON_TRACKING_LOST, OBJECT_CLICKED
        }

        public enum COMPONENT_TYPE
        {
            AUDIO_SOURCE, POSITIONS, TEXTS, BLEND, CLICK
        }

        #endregion

        #region CONSTRUCTOR

        public class ComponentTypeRequirement
        {
            public readonly COMPONENT_TYPE componentType;

            public readonly PERFORM_ACTION_DURING performActionDuring;

            private CCIActionsEventHandler _componentObject;
            public CCIActionsEventHandler componentObject
            {
                get
                {
                    return _componentObject;
                }
                protected set
                {
                    _componentObject = value;
                }
            }

            public readonly bool required = false;

            public ComponentTypeRequirement(COMPONENT_TYPE componentType, PERFORM_ACTION_DURING performActionDuring, CCIActionsEventHandler componentObject)
            //public ComponentTypeRequirement(COMPONENT_TYPE componentType, PERFORM_ACTION_DURING performActionDuring)
            {
                this.componentType = componentType;
                this.performActionDuring = performActionDuring;
                this.componentObject = componentObject;
                required = true;
            }
        }

        #endregion

        #region PRIVATE_METHODS

        private void AddCCComponent(COMPONENT_TYPE componentType, PERFORM_ACTION_DURING performActionDuring, CCIActionsEventHandler componentObject)
        //private void AddCCComponent(COMPONENT_TYPE componentType, PERFORM_ACTION_DURING performActionDuring)
        {
            //ComponentTypeRequirement ctr;

            //switch (_componentType)
            //{
            //    case COMPONENT_TYPE.AUDIO_SOURCE:
            //        AudioSource src = gameObject.AddComponent<AudioSource>();
            //        ctr = new ComponentTypeRequirement(_componentType, src);
            //        break;
            //    case COMPONENT_TYPE.POSITIONS:

            //        ctr = new ComponentTypeRequirement(_componentType, null);
            //        break;
            //    default:
            //        return;
            //}

            ComponentTypeRequirement ctr = new ComponentTypeRequirement(componentType, performActionDuring, componentObject);
            //ComponentTypeRequirement ctr = new ComponentTypeRequirement(componentType, performActionDuring);
            _componentTypeRequirements.Add(ctr);
        }

        #endregion

        #region PUBLIC_METHODS

        public ComponentTypeRequirement GetCCComponent(COMPONENT_TYPE componentType)
        {
            return componentTypeRequirements.Find(c => c.componentType.Equals(componentType));
        }

        #region GETTERS_AND_SETTERS

        public AudioSource getAudioSource()
        {
            return audioSource;
        }

        public AudioClip clip
        {
            get
            {
                return audioSource.clip;
            }
            set
            {
                audioSource.clip = value;
            }
        }

        public void play()
        {
            audioSource.Play();
        }

        public void playOneShot(AudioClip inClip)
        {
            audioSource.PlayOneShot(inClip);
        }

        public void playOneShot(AudioClip inClip, float inVolumeScale)
        {
            audioSource.PlayOneShot(inClip, inVolumeScale);
        }

        public void stop()
        {
            audioSource.Stop();
        }

        public void addText(string key, string text)
        {
            if(_texts == null)
            {
                return;
            }
            _texts.Add(key, text);
        }

        public void modifyText(string key, string newText)
        {
            if (_texts == null)
            {
                return;
            }
            _texts[key] = newText;
        }

        public string getText(string key)
        {
            if (_texts == null)
            {
                return null;
            }
            return _texts[key];
        }

        public void removeText(string key)
        {
            if (_texts == null)
            {
                return;
            }
            _texts.Remove(key);
        }

        public void addPosition(string key, Vector3 position)
        {
            _positions.Add(key, position);
        }

        public void modifyPosition(string key, Vector3 newPosition)
        {
            _positions[key] = newPosition;
        }

        public Vector3 getPosition(string key)
        {
            return _positions[key];
        }

        public void removePosition(string key)
        {
            _positions.Remove(key);
        }

        public void setBlendRate(float targetBlendRate, float blendRateSpeed)
        {
            _objectBlendMetadata.setBlendRate(targetBlendRate, blendRateSpeed);
        }

        //public void startBlend()
        //{
        //    _objectBlendMetadata.isBlend = true;
        //}

        #endregion

        #region SET_REQUIRES

        //public void setRequireAudioSource(PERFORM_ACTION_DURING performActionDuring)
        //{
        //    //audioSource = gameObject.AddComponent<AudioSource>();
        //    AddCCComponent(COMPONENT_TYPE.AUDIO_SOURCE, performActionDuring, gameObject.AddComponent<AudioSource>());
        //}

        public void setRequireAudioSource(PERFORM_ACTION_DURING performActionDuring, AudioClip audioClip)
        {
            CCAudioSource ccAudioSource = gameObject.AddComponent<CCAudioSource>();
            ccAudioSource.clip = audioClip;
            AddCCComponent(COMPONENT_TYPE.AUDIO_SOURCE, performActionDuring, ccAudioSource);
        }

        //public void setRequireTexts(PERFORM_ACTION_DURING performActionDuring)
        //{
        //    _texts = new Dictionary<string, string>();
        //    AddCCComponent(COMPONENT_TYPE.TEXTS, performActionDuring);
        //}

        //public void setRequirePositions(PERFORM_ACTION_DURING performActionDuring)
        //{
        //    _positions = new Dictionary<string, Vector3>();
        //    AddCCComponent(COMPONENT_TYPE.POSITIONS, performActionDuring);
        //}

        public void setRequireBlend(PERFORM_ACTION_DURING performActionDuring)
        {
            //_objectBlendMetadata = gameObject.AddComponent<CCObjectBlend>();
            AddCCComponent(COMPONENT_TYPE.BLEND, performActionDuring, gameObject.AddComponent<CCObjectBlend>());
        }

        public void setRequireBlend(PERFORM_ACTION_DURING performActionDuring, float targetBlendRate, float blendRateSpeed)
        {
            CCObjectBlend objectBlendMetadata = gameObject.AddComponent<CCObjectBlend>();
            objectBlendMetadata.setBlendRate(targetBlendRate, blendRateSpeed);
            AddCCComponent(COMPONENT_TYPE.BLEND, performActionDuring, objectBlendMetadata);
        }

        //public void setRequireFadeIn()
        //{
        //    _requireFadeIn = true;
        //}

        //public void setRequireFadeOut()
        //{
        //    _requireFadeOut = true;
        //}

        #endregion

        //public void attachRequiredComponents()
        //{
        //    if(requireAudioSource)
        //    {
        //        _audioSource = gameObject.AddComponent<AudioSource>();
        //    }
        //}

        #endregion

        #region VARIABLE_DECLARATION

        private List<ComponentTypeRequirement> _componentTypeRequirements = new List<ComponentTypeRequirement>();
        public List<ComponentTypeRequirement> componentTypeRequirements
        {
            get
            {
                return _componentTypeRequirements;
            }
        }

        private AudioSource _audioSource;
        public AudioSource audioSource
        {
            get
            {
                return _audioSource;
            }
            private set
            {
                _audioSource = value;
            }
        }

        // Dictionary of strings to be used
        private Dictionary<string, string> _texts;
        public Dictionary<string, string> texts
        {
            get
            {
                return _texts;
            }
        }

        // Dictionary of positions to be used for placement or movement
        private Dictionary<string, Vector3> _positions;
        public Dictionary<string, Vector3> positions
        {
            get
            {
                return _positions;
            }
        }

        private CCObjectBlend _objectBlendMetadata;
        public CCObjectBlend objectBlendMetadata
        {
            get
            {
                return _objectBlendMetadata;
            }
        }

        #endregion
    }
}
