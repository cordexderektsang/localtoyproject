﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;
using Vuforia;

namespace CCARPlatformFramework.CCDataHandling
{
    // Make it unextendable
    public sealed class CCDownloadController : MonoBehaviour
    {
        // Make it un-accessable
        private CCDownloadController()
        {
        }

        /// <summary>
        /// The Instance Obj
        /// </summary>
        private static CCDownloadController _self;
        public static CCDownloadController Instance
        {
            get
            {
                if (_self == null)
                {
                    // Need an obj to start coroutine
                    GameObject obj = new GameObject("CCDownloadManager");

                    // Keep download in background
                    DontDestroyOnLoad(obj);
                    _self = obj.AddComponent<CCDownloadController>();
                }

                return _self;
            }
            private set
            {
                _self = value;
            }
        }


        /// <summary>
        /// Downloads the multi files.
        /// </summary>
        /// <param name='urls'>
        /// Urls.
        /// </param>
        /// <param name='progressCallback'>
        /// Progress callback.
        /// </param>
        /// <param name='finishCallback'>
        /// Finish callback.
        /// </param>
        public void DownloadMultiFiles(List<string> urls, Action<float, string> progressCallback, Action<List<WWW>> finishCallback, Action failCallback)
        {
            StartCoroutine(downloadMultipleFiles(urls, progressCallback, finishCallback, failCallback));
        }

        /// <summary>
        /// Downloads the single file.
        /// </summary>
        /// <param name='url'>
        /// URL.
        /// </param>
        /// <param name='progressCallback'>
        /// Progress callback.
        /// </param>
        /// <param name='finishCallback'>
        /// Finish callback.
        /// </param>
        public void DownloadSingleFile(string url, Action<float, string> progressCallback, Action<WWW> finishCallback, Action failCallback)
        {
            StartCoroutine(downloadFile(url, progressCallback, finishCallback, failCallback));
        }

        /// <summary>
        /// Downloads multiple asset bundles.
        /// </summary>
        /// <param name='urls'>
        /// Urls.
        /// </param>
        /// <param name='names'>
        /// Names of markers.
        /// </param>
        /// <param name='versions'>
        /// Versions.
        /// </param>
        /// <param name='progressCallback'>
        /// Progress callback.
        /// </param>
        /// <param name='finishCallback'>
        /// Finish callback.
        /// </param>
        public void DownloadMultiAssetBundles(List<string> urls, List<string> names, List<string> versions, Action<float, string> progressCallback, Action<List<WWW>, List<string>, List<string>> finishCallback, Action failCallback)
        {
            //StartCoroutine(downloadMultipleAssetBundles(urls, names, versions, progressCallback, finishCallback));
            StartCoroutine(downloadMultipleFiles(urls, progressCallback, (List<WWW> wwwList) =>
            {
                finishCallback(wwwList, names, versions);
            },
            failCallback));
        }

        /// <summary>
        /// Downloads a single asset bundle.
        /// </summary>
        /// <param name='url'>
        /// URL.
        /// </param>
        /// <param name='name'>
        /// Name of marker.
        /// </param>
        /// <param name='version'>
        /// Version.
        /// </param>
        /// <param name='progressCallback'>
        /// Progress callback.
        /// </param>
        /// <param name='finishCallback'>
        /// Finish callback.
        /// </param>
        public void DownloadSingleAssetBundle(string url, string name, string version, Action<float, string> progressCallback, Action<WWW, string, string> finishCallback, Action failCallback)
        {
            StartCoroutine(downloadFile(url, progressCallback, (WWW www) =>
            {
                finishCallback(www, name, version);
            },
            failCallback));
        }

        [System.Reflection.Obfuscation(ApplyToMembers = false)]
        private IEnumerator downloadMultipleFiles(List<string> urls, Action<float, string> progressCallback, Action<List<WWW>> finishCallback, Action failCallback)
        {
            List<WWW> results = new List<WWW>(urls.Count);

            foreach (string url in urls)
            {
                // Call single download file
                yield return StartCoroutine(downloadFile(url, progressCallback, (WWW www) => { results.Add(www); }, failCallback));
            }

            yield return new WaitForEndOfFrame();

            if (finishCallback != null)
            {
                finishCallback(results);
            }
        }

        [System.Reflection.Obfuscation(ApplyToMembers = false)]
        private IEnumerator downloadFile(string url, Action<float, string> progressCallback, Action<WWW> finishCallback, Action failCallback)
        {
			System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0);
			int lastViewTime = (int)(System.DateTime.Now - epochStart).TotalSeconds;

			WWW www = new WWW(url + "?t=" + lastViewTime);
         //   Debug.Log("www_______1________________________________error " + www.error);
            if (progressCallback != null)
            {
           //     Debug.Log("www_______5________________________________isDone ");
                while (!www.isDone)
                {
             //       Debug.Log(www.isDone + "www_______2________________________________error " + www.error);
                    if (string.IsNullOrEmpty(www.error)/* && (!url.Contains("930811") || downloadCatalogAsset.DieCount > 2)*/)
                    {
                        progressCallback(www.progress, url);
                        yield return null;
                    }
                    else
                    {
                //        Debug.Log("www___________3____________________________error " + www.error);
                        //downloadCatalogAsset.DieCount++;
                        failCallback();
                        yield break;
                    }
                }
                if (string.IsNullOrEmpty(www.error)/* && (!url.Contains("930811") || downloadCatalogAsset.DieCount > 2)*/)
                {
                    progressCallback(www.progress, url);
                    yield return null;
                }
                else
                {
                  //  Debug.Log("www___________3____________________________error " + www.error);
                    Debug.LogWarning("www failed for URL: " + url);
                    //downloadCatalogAsset.DieCount++;
                    failCallback();
                    yield break;
                }
            }
            else
            {
                //Debug.Log(www.isDone + "www_______4________________________________error " + www.error);
                //while (!www.isDone)
                //{
                //    Debug.Log("www_______2________________________________error " + www.error);
                //    if (!string.IsNullOrEmpty(www.error)/* && (!url.Contains("930811") || downloadCatalogAsset.DieCount > 2)*/)
                //    {
                //        failCallback();
                //    }
                //}
                yield return www;
            }

            yield return new WaitForEndOfFrame();
            //Debug.Log("download file finish " + www.error);
            if (finishCallback != null)
            {
                finishCallback(www);
            }
        }
    }
}