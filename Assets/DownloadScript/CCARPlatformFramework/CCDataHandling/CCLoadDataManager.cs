﻿using CCARPlatformFramework.CCConstants;
using CCARPlatformFramework.CCObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Vuforia;

namespace CCARPlatformFramework.CCDataHandling
{
    public class CCLoadDataManager : MonoBehaviour
    {
        private static ObjectTracker ObjectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
		public static float currentProgressPercetnage;
      //  private static MarkerTracker MarkerTracker = TrackerManager.Instance.GetTracker<MarkerTracker>();

        /// <summary>
        /// Load all message data in the provided path
        /// </summary>
        /// <param name="metadataName">name of metadata file to load</param>
        /// <param name="metadataPath">string of path of metadata location</param>
        /// <returns>Message metadata</returns>
        public static CCMessageMetadata LoadMessageMetadataJSON(string metadataName, string metadataPath)
        {
            CCMessageMetadata returnMetadata = new CCMessageMetadata();

            string metadataFilename = Path.Combine(metadataPath, metadataName + "Message.json");
            string jsonText = File.ReadAllText(metadataFilename);
            string metadataVersionFilename = metadataFilename.Replace(".json", "." + CCJSONConstants.VERSION);

            string qcarName;
            string metadataVersion;

            //Debug.Log("DEBUG: jsonData: " + jsonText);

            // Deserialize the json text to object
            IDictionary jsonData = (IDictionary)(MiniJSON.Json.Deserialize(jsonText));

            // Load message component
            IDictionary messageData = (IDictionary)jsonData[CCJSONConstants.MESSAGE];

            qcarName = Convert.ToString(messageData[CCJSONConstants.QCAR_NAME]);
            metadataVersion = Convert.ToString(messageData[CCJSONConstants.VERSION]); // global metadata version

            returnMetadata.qcarDownloadName = qcarName;

            // Check and compare metadata version
            if (File.Exists(metadataVersionFilename))
            {
                //Debug.Log("DEBUG: existing message metadata version file found");
                string fileMetadataVersion = File.ReadAllText(metadataVersionFilename);
                if (CheckIfNewVersionIsNewer(fileMetadataVersion, metadataVersion))
                {
                    //Debug.Log("DEBUG: new message metadata version is newer, need to redownload all");
                    returnMetadata.metadataUpdated = true;
                }
            }
            else
            {
                //Debug.Log("DEBUG: existing message metadata version file not found");
                returnMetadata.metadataUpdated = true;
            }
            
            if (returnMetadata.metadataUpdated)
            {
                File.WriteAllText(metadataVersionFilename, metadataVersion);
            }

            return returnMetadata;
        }

        /// <summary>
        /// Load all bundle data in the provided path
        /// </summary>
        /// <param name="metadataName">name of metadata file to load</param>
        /// <param name="metadataPath">string of path of metadata location</param>
        public static Dictionary<string, CCBundleMetadata> LoadBundleMetadataJSON(string metadataName, string metadataPath)
        {
            Dictionary<string, CCBundleMetadata> returnDic = new Dictionary<string, CCBundleMetadata>();

            string metadataFilename = Path.Combine(metadataPath, metadataName + "Bundles.json");
            string jsonText = File.ReadAllText(metadataFilename);

            Debug.Log("DEBUG: jsonData: " + jsonText);

            // Deserialize the json text to object
            IDictionary jsonData = (IDictionary)(MiniJSON.Json.Deserialize(jsonText));
            // Get all bundles information


            IList bundles = (IList)jsonData[CCJSONConstants.BUNDLES];

            for (int i = 0; i < bundles.Count; i++)
            {
                CCBundleMetadata bundleMetadata = new CCBundleMetadata();

                IDictionary bundleData = (IDictionary)bundles[i];
               // Debug.Log("DEBUG: bundledata[" + i + "] " + CCJSONConstants.BUNDLE_NAME + ": " + bundleData[CCJSONConstants.BUNDLE_NAME]);
                bundleMetadata.downloadName = Convert.ToString(bundleData[CCJSONConstants.BUNDLE_NAME]);

#if UNITY_ANDROID
                bundleMetadata.downloadName = bundleMetadata.downloadName + "android";
       
#elif UNITY_IOS
                bundleMetadata.downloadName = bundleMetadata.downloadName + "ios";
#endif

                //Debug.Log("DEBUG: bundleMetadata.bundleDownloadName[" + i + "]: " + bundleMetadata.downloadName);

                //Debug.Log("DEBUG: bundledata[" + i + "] " + CCJSONConstants.VERSION + ": " + bundleData[CCJSONConstants.VERSION]);
                bundleMetadata.version = Convert.ToString(bundleData[CCJSONConstants.VERSION]);

                string bundleVersionFilename = Path.Combine(Path.Combine(metadataPath, "assetBundles"), bundleMetadata.downloadName + "." + CCJSONConstants.VERSION);

                // Check and compare metadata version
                if (File.Exists(bundleVersionFilename))
                {
                    //Debug.Log("DEBUG: existing bundle metadata version file found");
                    string fileBundleVersion = File.ReadAllText(bundleVersionFilename);
                    if (CheckIfNewVersionIsNewer(fileBundleVersion, bundleMetadata.version))
                    {
                        //Debug.Log("DEBUG: new bundle metadata version is newer, need to redownload");
                        bundleMetadata.updated = true;
                    }
                }
                else
                {
                    //Debug.Log("DEBUG: existing bundle metadata version file not found");
                    bundleMetadata.updated = true;
                }

                returnDic.Add(bundleMetadata.downloadName, bundleMetadata);

                //if (bundleMetadata.bundleUpdated)
                //{
                //    File.WriteAllText(bundleVersionFilename, metadataVersion);
                //}
            }

            return returnDic;
        }

        /// <summary>
        /// Load all Lua data in the provided path
        /// </summary>
        /// <param name="metadataName">name of metadata file to load</param>
        /// <param name="metadataPath">string of path of metadata location</param>
        public static Dictionary<string, CCLuaMetadata> LoadLuaMetadataJSON(string metadataName, string metadataPath)
        {
            Dictionary<string, CCLuaMetadata> returnDic = new Dictionary<string, CCLuaMetadata>();

            string metadataFilename = Path.Combine(metadataPath, metadataName + "Lua.json");
            string jsonText = File.ReadAllText(metadataFilename);

            //Debug.Log("DEBUG: jsonData: " + jsonText);

            // Deserialize the json text to object
            IDictionary jsonData = (IDictionary)(MiniJSON.Json.Deserialize(jsonText));

            // Get all Lua files information
            IList luaFiles = (IList)jsonData[CCJSONConstants.LUA_FILES];

            for (int i = 0; i < luaFiles.Count; i++)
            {
                CCLuaMetadata luaMetadata = new CCLuaMetadata();

                IDictionary luaData = (IDictionary)luaFiles[i];
                //Debug.Log("DEBUG: luaData[" + i + "] " + CCJSONConstants.SCRIPT_NAME + ": " + luaData[CCJSONConstants.SCRIPT_NAME]);
                luaMetadata.downloadName = Convert.ToString(luaData[CCJSONConstants.SCRIPT_NAME]);

               //Debug.Log("DEBUG: luaData[" + i + "] " + CCJSONConstants.PARENT + ": " + luaData[CCJSONConstants.PARENT]);
                luaMetadata.parentGameObjectName = Convert.ToString(luaData[CCJSONConstants.PARENT]);

               //Debug.Log("DEBUG: luaData[" + i + "] " + CCJSONConstants.VERSION + ": " + luaData[CCJSONConstants.VERSION]);
                luaMetadata.version = Convert.ToString(luaData[CCJSONConstants.VERSION]);

                string luaVersionFilename = Path.Combine(Path.Combine(metadataPath, "luaFiles"), luaMetadata.downloadName + "." + CCJSONConstants.VERSION);
                // Check and compare metadata version
                if (File.Exists(luaVersionFilename))
                {
                    //Debug.Log("DEBUG: existing lua metadata version file found");
                    string fileBundleVersion = File.ReadAllText(luaVersionFilename);
                    if (CheckIfNewVersionIsNewer(fileBundleVersion, luaMetadata.version))
                    {
                       // Debug.Log("DEBUG: new lua metadata version is newer, need to redownload");
                        luaMetadata.updated = true;
                    }
                }
                else
                {
                    Debug.Log("DEBUG: existing lua metadata version file not found");
                    luaMetadata.updated = true;
                }

                returnDic.Add(luaMetadata.downloadName, luaMetadata);
            }

            return returnDic;
        }

        /// <summary>
        /// Load all markerMetadata in the provided path
        /// </summary>
        /// <param name="messageMetadataUpdated">boolean to check if global metadata version has changed</param>
        /// <param name="metadataName">name of metadata file to load</param>
        /// <param name="metadataPath">string of path of metadatas location</param>
        /// <returns>Dictionary of result of each loaded metadata</returns>
        public static Dictionary<string, CCMarkerMetadata> LoadMarkerMetadataJSON(bool messageMetadataUpdated, string metadataName, string metadataPath)
        {
            Dictionary<string, CCMarkerMetadata> returnDic = new Dictionary<string, CCMarkerMetadata>();

            string metadataFilename = Path.Combine(metadataPath, metadataName + "Markers.json");
            string jsonText = File.ReadAllText(metadataFilename);


            Debug.Log("DEBUG: jsonData: " + jsonText);

            // Deserialize the json text to object
            IDictionary jsonData = (IDictionary)(MiniJSON.Json.Deserialize(jsonText));

            // Load markers component
            IList markers = (IList)jsonData[CCJSONConstants.MARKERS];

            //Debug.Log("DEBUG: IList markers[0]: " + markers[0]);

            for (int i = 0; i < markers.Count; i++)
            {
                CCMarkerMetadata markerMetadata = new CCMarkerMetadata();

                IDictionary markerData = (IDictionary)markers[i];
                if (markerData.Contains(CCJSONConstants.ID))
                {
                    Debug.Log("DEBUG: markerdata[" + i + "] " + CCJSONConstants.ID + ": " + markerData[CCJSONConstants.ID]);
                    markerMetadata.id = Convert.ToInt32(markerData[CCJSONConstants.ID]);
                }

                if (markerData.Contains(CCJSONConstants.MARKER_NAME))
                {
                    Debug.Log("DEBUG: markerdata[" + i + "] " + CCJSONConstants.MARKER_NAME + ": " + markerData[CCJSONConstants.MARKER_NAME]);
                    markerMetadata.markerName = Convert.ToString(markerData[CCJSONConstants.MARKER_NAME]);
                }

                if (markerData.Contains(CCJSONConstants.MARKER_PROPERTIES))
                {
                  //  Debug.Log("DEBUG: markerdata[" + i + "] " + CCJSONConstants.MARKER_VARIABLES + ": " + markerData[CCJSONConstants.MARKER_VARIABLES]);

                    //IList variablesList = (IList)markerData[CCJSONConstants.MARKER_VARIABLES];
                    //IDictionary variablesData = (IDictionary)variablesList[0]; // only one MarkerVariables component
                    IDictionary markerProperties = (IDictionary)markerData[CCJSONConstants.MARKER_PROPERTIES];

                    //string markerVersionName = markerMetadata.name + CCJSONConstants.VERSION;
                    //string markerVersionFilename = Application.persistentDataPath + "/" + markerMetadata.name + "/" + CCJSONConstants.VERSION;
                    string markerVersionFilename = Path.Combine(Path.Combine(metadataPath, markerMetadata.markerName), markerMetadata.markerName + "." + CCJSONConstants.VERSION);
                    string markerVersion = Convert.ToString(markerProperties[CCJSONConstants.VERSION]); // marker version
                    string fileMarkerVersion = "";

                    if (File.Exists(markerVersionFilename))
                    {
                        //Debug.Log("DEBUG: markerdata[" + i + "] existing marker version file found");
                        fileMarkerVersion = File.ReadAllText(markerVersionFilename);
                        if (messageMetadataUpdated || CheckIfNewVersionIsNewer(fileMarkerVersion, markerVersion))
                        {
                            //Debug.Log("DEBUG: markerdata[" + i + "] new marker version is newer, need to redownload");
                            markerMetadata.markerNeedDownload = true;
                        }
                    }
                    else
                    {
                        //Debug.Log("DEBUG: markerdata[" + i + "] existing marker version file not found");
                        markerMetadata.markerNeedDownload = true;
                    }

                    if (markerMetadata.markerNeedDownload)
                    {
                        markerMetadata.markerVersion = markerVersion;
                    }
                    else
                    {
                        markerMetadata.markerVersion = fileMarkerVersion;
                    }

                    if (markerProperties.Contains(CCJSONConstants.POSITION))
                    {
                        //Debug.Log("DEBUG: markerdata[" + i + "] " + CCJSONConstants.POSITION + ": " + objectPropertiesData[CCJSONConstants.POSITION]);
                        markerMetadata.markerPosition = LoadVector3Data(markerProperties, CCJSONConstants.POSITION);
                    }

                    if (markerProperties.Contains(CCJSONConstants.ROTATION))
                    {
                        //Debug.Log("DEBUG: markerdata[" + i + "] " + CCJSONConstants.ROTATION + ": " + objectPropertiesData[CCJSONConstants.ROTATION]);
                        markerMetadata.markerRotation = LoadVector3Data(markerProperties, CCJSONConstants.ROTATION);
                    }

                    if (markerProperties.Contains(CCJSONConstants.SCALING))
                    {
                        //Debug.Log("DEBUG: markerdata[" + i + "] " + CCJSONConstants.ROTATION + ": " + objectPropertiesData[CCJSONConstants.ROTATION]);
                        markerMetadata.markerScale = LoadVector3Data(markerProperties, CCJSONConstants.SCALING);
                    }

                    if (markerProperties.Contains(CCJSONConstants.TRACKABLE_HANDLER_NAME))
                    {
                        //Debug.Log("DEBUG: markerdata[" + i + "] " + CCJSONConstants.SCALING + ": " + objectPropertiesData[CCJSONConstants.SCALING]);
                        markerMetadata.trackableHandlerName = Convert.ToString(markerProperties[CCJSONConstants.TRACKABLE_HANDLER_NAME]);
                    }


                }

                if (markerData.Contains(CCJSONConstants.TYPE))
                {
                    //Debug.Log("DEBUG: markerdata[" + i + "] " + CCJSONConstants.TYPE + ": " + markerData[CCJSONConstants.TYPE]);
                    markerMetadata.type = (CCMarkerType)Enum.Parse(typeof(CCMarkerType), Convert.ToString(markerData[CCJSONConstants.TYPE]));
                }

                if (markerData.Contains(CCJSONConstants.BUNDLE_NAME))
                {
                    //Debug.Log("DEBUG: markerdata[" + i + "] " + CCJSONConstants.BUNDLE_NAME + ": " + markerData[CCJSONConstants.BUNDLE_NAME]);
                    string bundleName = Convert.ToString(markerData[CCJSONConstants.BUNDLE_NAME]);
#if UNITY_ANDROID
                    markerMetadata.bundleName = bundleName + "android";
#elif UNITY_IOS
                    markerMetadata.bundleName = bundleName + "ios";
#endif
                }

                if (markerData.Contains(CCJSONConstants.MARKER_OBJECT_NAME))
                {
                    //Debug.Log("DEBUG: markerdata[" + i + "] " + CCJSONConstants.MARKER_OBJECT_NAME + ": " + markerData[CCJSONConstants.MARKER_OBJECT_NAME]);
                    markerMetadata.markerObjectName = Convert.ToString(markerData[CCJSONConstants.MARKER_OBJECT_NAME]);
                }

                if (markerData.Contains(CCJSONConstants.MARKER_OBJECT_PROPERTIES))
                {
                    Debug.Log("DEBUG: markerdata[" + i + "] " + CCJSONConstants.MARKER_OBJECT_PROPERTIES + ": " + markerData[CCJSONConstants.MARKER_OBJECT_PROPERTIES]);

                    IDictionary objectPropertiesData = (IDictionary)markerData[CCJSONConstants.MARKER_OBJECT_PROPERTIES];

                    if (objectPropertiesData.Contains(CCJSONConstants.POSITION))
                    {
                        //Debug.Log("DEBUG: markerdata[" + i + "] " + CCJSONConstants.POSITION + ": " + objectPropertiesData[CCJSONConstants.POSITION]);
                        markerMetadata.markerObjectPosition = LoadVector3Data(objectPropertiesData, CCJSONConstants.POSITION);
                    }

                    if (objectPropertiesData.Contains(CCJSONConstants.ROTATION))
                    {
                        //Debug.Log("DEBUG: markerdata[" + i + "] " + CCJSONConstants.ROTATION + ": " + objectPropertiesData[CCJSONConstants.ROTATION]);
                        markerMetadata.markerObjectRotation = LoadVector3Data(objectPropertiesData, CCJSONConstants.ROTATION);
                    }

                    if (objectPropertiesData.Contains(CCJSONConstants.SCALING))
                    {
                        //Debug.Log("DEBUG: markerdata[" + i + "] " + CCJSONConstants.SCALING + ": " + objectPropertiesData[CCJSONConstants.SCALING]);
                        markerMetadata.markerObjectScale = LoadVector3Data(objectPropertiesData, CCJSONConstants.SCALING);
                    }

                    if (objectPropertiesData.Contains(CCJSONConstants.MATERIAL_NAME))
                    {
                        Debug.Log("DEBUG: markerdata[" + i + "] " + CCJSONConstants.MATERIAL_NAME + ": " + objectPropertiesData[CCJSONConstants.MATERIAL_NAME]);
                        markerMetadata.markerObjectMaterialName = Convert.ToString(objectPropertiesData[CCJSONConstants.MATERIAL_NAME]);
                    }

                    if (objectPropertiesData.Contains(CCJSONConstants.SHADER_NAME))
                    {
                        Debug.Log("DEBUG: markerdata[" + i + "] " + CCJSONConstants.SHADER_NAME + ": " + objectPropertiesData[CCJSONConstants.SHADER_NAME]);
                        markerMetadata.markerObjectShaderName = Convert.ToString(objectPropertiesData[CCJSONConstants.SHADER_NAME]);
                    }
                }

                if (markerData.Contains(CCJSONConstants.OTHER))
                {
                    //Debug.Log("DEBUG: markerdata[" + i + "] " + CCJSONConstants.OTHER + ": " + markerData[CCJSONConstants.OTHER]);
                    markerMetadata.other = Convert.ToString(markerData[CCJSONConstants.OTHER]);
                }

                // Parse components later when loading marker objects
                if (markerData.Contains(CCJSONConstants.COMPONENTS))
                {
                    //Debug.Log("DEBUG: markerdata[" + i + "] " + CCJSONConstants.COMPONENTS + ": " + markerData[CCJSONConstants.COMPONENTS]);

                    markerMetadata.components = (IList)markerData[CCJSONConstants.COMPONENTS];
                }

                returnDic.Add(markerMetadata.markerName, markerMetadata);
            }

            return returnDic;
        }

        /// <summary>
        /// Returns a listing of all dataSets in the given path
        /// </summary>
        /// <param name="dataSetPath">String of path of dataSets location</param>
        /// <returns>List of dataSets returned in the path</returns>
        public static List<string> RetrieveDataSets(string dataSetPath)
        {
            string[] files = Directory.GetFiles(dataSetPath, "*.xml");
            return files.ToList();
        }

        /// <summary>
        /// Load all dataSets in the provided path
        /// </summary>
        /// <param name="dataSetPaths">string of path of dataSets location</param>
        /// <returns>Dictionary of result of each loaded/not loaded dataSet</returns>
        public static Dictionary<string, bool> LoadMarkerDataSet(string dataSetPath)
        {
            //ObjectTracker ObjectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            //MarkerTracker MarkerTracker = TrackerManager.Instance.GetTracker<MarkerTracker>();
            Debug.Log(dataSetPath);
            Dictionary<string, bool> returnDic = new Dictionary<string, bool>();

            string[] dataSetPaths = Directory.GetFiles(dataSetPath, "*.xml");

            // stop trackers so that we can add new dataSets
            //StopTrackers();
            //if (ObjectTracker.IsActive)
            //{
           // ObjectTracker.Stop();
            //}

            //if (MarkerTracker.IsActive)
            //{
           // MarkerTracker.Stop();
            //}

            //Debug.Log("objectTracker active: " + ObjectTracker.IsActive);
            //Debug.Log("markerTracker active: " + MarkerTracker.IsActive);

           /* foreach (string dataSetPathName in dataSetPaths)
            {
                //Debug.Log("DEBUG: LoadMarkerDataSet(multiple), dataSetPathName: " + dataSetPathName);

                DataSet dataSet = ObjectTracker.CreateDataSet();

                string assetName = GetAssetName(dataSetPathName, ".xml");

                //Debug.Log("DEBUG: dataset exists: " + DataSet.Exists(dataSetPathName, VuforiaUnity.StorageType.STORAGE_ABSOLUTE));

                if (dataSet.Load(dataSetPathName, VuforiaUnity.StorageType.STORAGE_ABSOLUTE))
                {
                    if (!ObjectTracker.ActivateDataSet(dataSet))
                    {
                        //Debug.Log("DEBUG: dataSet not activated");
                        // Note: ImageTracker cannot have more than 100 total targets activated
                        returnDic.Add(assetName, false);
                    }

                    returnDic.Add(assetName, true);
                }
                else {
                    //Debug.Log("DEBUG: dataSet not loaded");
                    returnDic.Add(assetName, false);
                }
            }

            //StartTrackers();
            if (!MarkerTracker.IsActive)
            {
                MarkerTracker.Start();
            }

            if (!ObjectTracker.IsActive)
            {
                ObjectTracker.Start();
            }*/

            return returnDic;
        }

        /// <summary>
        /// Returns a listing of all metadatas in the given path
        /// </summary>
        /// <param name="metadataPath">String of path of metadatas location</param>
        /// <returns>List of metadatas returned in the path</returns>
        public static List<string> RetrieveMetadatas(string metadataPath)
        {
            string[] files = Directory.GetFiles(metadataPath, "*.json");
            return files.ToList();
        }

        /// <summary>
        /// Check version differences between existing and new versions
        /// </summary>
        /// <param name="existingVersionIn">Existing version number</param>
        /// <param name="newVersionIn">New version number</param>
        /// <returns></returns>
        private static bool CheckIfNewVersionIsNewer(string existingVersionIn, string newVersionIn)
        {
            int existingVersionDotCount = existingVersionIn.Count(dot => dot.Equals('.'));
            int newVersionDotCount = newVersionIn.Count(dot => dot.Equals('.'));

            string existingVersion = existingVersionIn;
            string newVersion = newVersionIn;

            // Sanity check if version has no subversions (i.e. a version jump from 1.1.0.3 to 2), append one subversion .0 (version 2 becomes 2.0)
            if (existingVersionDotCount == 0)
            {
                existingVersion = existingVersion + ".0";
                existingVersionDotCount = 1;
            }

            if (newVersionDotCount == 0)
            {
                newVersion = newVersion + ".0";
                newVersionDotCount = 1;
            }

            string[] existingVersions = existingVersion.Split('.');
            string[] newVersions = newVersion.Split('.');

            // If same number of subversions, compare directly the integers
            if (existingVersionDotCount == newVersionDotCount)
            {
                return GenericVersionChecker(existingVersionDotCount + 1, newVersions, existingVersions);
            }
            // If current version has more subversions, check from the least common digit
            else if (existingVersionDotCount > newVersionDotCount)
            {
                return GenericVersionChecker(newVersionDotCount + 1, newVersions, existingVersions);
            }
            // If new version has more subversions, check from the least common digit
            else
            {
                return GenericVersionChecker(existingVersionDotCount + 1, newVersions, existingVersions);
            }
        }

        /// <summary>
        /// Helper function to check if a subversion is larger or smaller
        /// </summary>
        /// <param name="loopCount">Number of loops based on length of version number</param>
        /// <param name="compareNewerVersions">Left operand where the values are checking greater to</param>
        /// <param name="compareOlderVersions">Right operand where the values are checking lesser to</param>
        /// <returns></returns>
        private static bool GenericVersionChecker(int loopCount, string[] compareNewerVersions, string[] compareOlderVersions)
        {
            for (int i = 0; i < loopCount; i++)
            {
                //Debug.Log("compareGreaterVersions[" + i + "]: " + compareGreaterVersions[i]);
                //Debug.Log("compareLesserVersions[" + i + "]: " + compareLesserVersions[i]);
                // If any 'larger' version is larger, immediately return true
                if (Convert.ToInt32(compareNewerVersions[i]) > Convert.ToInt32(compareOlderVersions[i]))
                {
                    return true;
                }
            }
            // If no subversions are greater, then it is same or older
            return false;
        }

        private static string GetAssetName(string assetPathName, string extension)
        {
            //Debug.Log("DEBUG: assetPathName.LastIndexOf(/) + 1: " + (assetPathName.LastIndexOf("/") + 1));
            //Debug.Log("DEBUG: assetPathName.IndexOf(extension): " + assetPathName.IndexOf(extension));

            //int start = assetPathName.LastIndexOf("/") + 1;
            int start = assetPathName.LastIndexOf(Path.DirectorySeparatorChar) + 1;
            int length = assetPathName.IndexOf(extension) - start;

            return assetPathName.Substring(start, length);
        }

        public static Vector3 LoadVector3Data(IDictionary data, string field)
        {
            Vector3 returnVec = new Vector3();

            //Debug.Log("DEBUG: parsing " + field);

            if (data.Contains(field))
            {
                returnVec.x = Convert.ToSingle(((IList)data[field])[0]);
                returnVec.y = Convert.ToSingle(((IList)data[field])[1]);
                returnVec.z = Convert.ToSingle(((IList)data[field])[2]);
            }

            return returnVec;
        }

        private static Vector3 LoadVector3Data(IList _list)
        //private static Vector3 LoadVector3Data(IDictionary _dic)
        {
            Vector3 returnVec = new Vector3();

            returnVec.x = Convert.ToSingle(_list[0]);
            returnVec.y = Convert.ToSingle(_list[1]);
            returnVec.z = Convert.ToSingle(_list[2]);

            //Debug.Log("DEBUG: " + System.Convert.ToSingle(fa[0]));

            return returnVec;
        }

     /*   private static void StopTrackers()
        {
            //ObjectTracker ObjectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            //MarkerTracker MarkerTracker = TrackerManager.Instance.GetTracker<MarkerTracker>();

            if (ObjectTracker != null)
            {
                ObjectTracker.Stop();
            }

            if (MarkerTracker != null)
            {
                MarkerTracker.Stop();
            }
        }

        private static void StartTrackers()
        {
            //ObjectTracker ObjectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            //MarkerTracker MarkerTracker = TrackerManager.Instance.GetTracker<MarkerTracker>();

            if (MarkerTracker != null)
            {
                MarkerTracker.Start();
            }

            if (ObjectTracker != null)
            {
                ObjectTracker.Start();
            }
        }*/
		public delegate void ProgressFunction (float progress, string url);
		public static ProgressFunction downloadFunction;

        public static void DownloadProgress(float downloadProgress, string url)
        {
			if (downloadFunction != null) {
				downloadFunction (downloadProgress, url);
			}
           //Debug.Log("DEBUG: Downloading progress at: " + (downloadProgress * 100) + ", url: " + url);
        }
    }
}
