﻿using System.Collections.Generic;
using UnityEngine;

namespace CCARPlatformFramework.CCTools
{
    public class CCLuaComponentVariableHelper : MonoBehaviour
    {
        public string SingleVariableUse;
        public List<string> MultiVariableUse;
    }
}