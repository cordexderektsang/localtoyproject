﻿using UnityEngine;

namespace CCARPlatformFramework.CCObjects
{
    public abstract class CCClickableObjectHandler : MonoBehaviour
    {
        public abstract void onClick();
    }
}