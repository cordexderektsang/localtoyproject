﻿namespace CCARPlatformFramework.CCObjects
{
    public class CCLuaMetadata
    {
        public string downloadName;
        public string version;
        public string parentGameObjectName;
        public string scriptLua;
        public bool updated = false;
    }
}