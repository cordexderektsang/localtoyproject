﻿using CCARPlatformFramework.CCInterfaces;
using CCARPlatformFramework.CCTools;
using UnityEngine;

namespace CCARPlatformFramework.CCObjects
{
    public class CCObjectBlend : MonoBehaviour, CCIActionsEventHandler
    {
        private Material material;

        private float _targetBlendRate;
        public float targetBlendRate
        {
            get
            {
                return _targetBlendRate;
            }
        }

        private float _blendRateSpeed;
        public float blendRateSpeed
        {
            get
            {
                return _blendRateSpeed;
            }
        }

        private float _originalBlendRate;
        public float originalBlendRate
        {
            get
            {
                return _originalBlendRate;
            }
        }

        private bool _isBlend = false;
        public bool isBlend
        {
            get
            {
                return _isBlend;
            }
        }
        //private Color color;

        public void reset()
        {
            material.SetFloat("_BlendRate", originalBlendRate);
        }

        public void setBlendRate(float targetBlendRate, float blendRateSpeed)
        {
            _targetBlendRate = targetBlendRate;
            _blendRateSpeed = blendRateSpeed;

            Renderer ren = gameObject.GetComponent<Renderer>();
            //color = ren.material.color;
            material = ren.material;

            _originalBlendRate = material.GetFloat("_BlendRate");
        }

        public void OnTrackingFoundEvent()
        {
            _isBlend = true;
        }

        public void OnTrackingLostEvent()
        {
            _isBlend = false;
            reset();
        }

        void Update()
        {
            if(isBlend)
            {
                //Debug.Log("DEBUG: Blending object: " + gameObject.name);
                float currBlendRate = material.GetFloat("_BlendRate");
                if (currBlendRate == targetBlendRate)
                {
                    _isBlend = false;
                }
                else
                {
                    material.SetFloat("_BlendRate", CCFadeBlendRate.BlendTowards(currBlendRate, targetBlendRate, blendRateSpeed));
                }
                //color = CCFadeColor.AlphaFade(color, targetBlendRate, blendRateSpeed);
                //if (color.a == targetBlendRate)
                //{
                //    isBlend = false;
                //}
            }
        }
    }
}