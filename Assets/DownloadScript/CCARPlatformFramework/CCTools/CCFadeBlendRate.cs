﻿using UnityEngine;

namespace CCARPlatformFramework.CCTools
{
    public class CCFadeBlendRate
    {
        public static float BlendTowards(float currBlendRate, float targetBlendRate, float blendRateSpeed)
        {
            if (currBlendRate != targetBlendRate)
            {
                return Mathf.MoveTowards(currBlendRate, targetBlendRate, blendRateSpeed * Time.deltaTime);
            }
            return currBlendRate;
        }
    }
}