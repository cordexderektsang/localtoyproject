﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vuforia;
using System.IO;
using System.Xml.Serialization;

public class FurnitureMarkerMenuItems : EditorWindow
{
    private static readonly string[] OBJECT_EXTENSIONS =
        {"fbx", "prefab"};
    private static readonly string[] MATERIAL_EXTENSIONS =
        {"mat"};
    private static readonly string[] TEXTURE_EXTENSIONS =
        {"png", "jpg"};

    private static Dictionary<string, AssetBundlesProperties> DictOfAssets = new Dictionary<string, AssetBundlesProperties>();
    private static List<AssetBundlesProperties> ListOfAssetsToBuild;

    private Vector2 scrollPos;
    private static bool IsMarkerObjectInit = false;
    private static List<string> ListOfMarkerNames;
    private static int SelectedMarkerIndex;
    private static List<string> ListOfChildObjectNames;

    private static ImageTargetBehaviour MarkerObject;
    private static List<string> MarkerNames;
    private static List<string> ListOfFurnitureNames;

    public static AssetBundlesMenuItems Instance { get; private set; }

    [MenuItem("Assets/Furniture JSON/Build JSON...")]
    public static void Init()
    {
        SetupData();

        //window.titleContent.text = "Asset Bundles";
        //window.Show();

        ShowWindow();
    }

    public static void ShowWindow()
    {
        GetWindow<FurnitureMarkerMenuItems>("Furniture JSON Files");
    }

    private static void SetupData(bool isReset = false)
    {
        ListOfFurnitureNames = new List<string>();

        //if (!IsMarkerObjectInit || isReset)
        if (!IsMarkerObjectInit || isReset)
        {
            IsMarkerObjectInit = true;

            ListOfMarkerNames = new List<string>();
            SelectedMarkerIndex = 0;
            ListOfChildObjectNames = new List<string>{ "", "", "", "", "", "", "", "", "", "" };

            string[] markerDatabaseXMLFiles = Directory.GetFiles(Path.Combine(Application.streamingAssetsPath, "QCAR"), "*.xml");

            foreach (string markerDatabaseXMLFile in markerDatabaseXMLFiles)
            {
                Debug.Log("xml file: " + markerDatabaseXMLFile);

                XmlSerializer serializer = new XmlSerializer(typeof(MarkerContainer));
                FileStream stream = new FileStream(markerDatabaseXMLFile, FileMode.Open);
                MarkerContainer container = serializer.Deserialize(stream) as MarkerContainer;
                stream.Close();

                foreach(Marker marker in container.markers)
                {
                    Debug.Log("marker name: " + marker.name + ", marker size: " + marker.sizeString);
                    //string[] sizeStringArray = marker.sizeString.Split(' ');
                    //marker.sizeVector = new Vector2(float.Parse(sizeStringArray[0]), float.Parse(sizeStringArray[1]));

                    ListOfMarkerNames.Add(marker.name);
                }
            }
        }

        //// Setup dictionary of AssetBundles to generate
        //string[] names = AssetDatabase.GetAllAssetBundleNames();
        //foreach (string name in names)
        //{
        //    //Debug.Log("AssetBundle: " + name);
        //    string[] assetPaths = AssetDatabase.GetAssetPathsFromAssetBundle(name);

        //    AssetBundlesProperties assetBundleProperties;

        //    if (!DictOfAssets.TryGetValue(name, out assetBundleProperties))
        //    {
        //        assetBundleProperties = new AssetBundlesProperties();
        //        assetBundleProperties.assetBundleName = name;
        //    }
        //    else
        //    {
        //        assetBundleProperties.assets = new List<string>();
        //        assetBundleProperties.assetCounts = new List<int> { 0, 0, 0, 0 };
        //        assetBundleProperties.JSONData = new List<string>();
        //    }

        //    foreach (string assetPath in assetPaths)
        //    {
        //        //assets.Add(assetPath);
        //        //Debug.Log("AssetPath: " + assetPath);
        //        assetBundleProperties.assets.Add(assetPath);
        //    }

        //    if (!DictOfAssets.ContainsKey(name) && assetPaths.Length > 0)
        //    {
        //        DictOfAssets.Add(name, assetBundleProperties);
        //    }
        //    else if (assetPaths.Length == 0)
        //    {
        //        DictOfAssets.Remove(name);
        //    }
        //}

        ////foreach (KeyValuePair<string, List<string>> assetKvp in DictOfAssets)
        //foreach (KeyValuePair<string, AssetBundlesProperties> assetKvp in DictOfAssets)
        //{
        //    //assetKvp.Value.isBuildAssetBundle = EditorGUILayout.Toggle(assetKvp.Key, assetKvp.Value.isBuildAssetBundle, GUILayout.MaxWidth(220.0f));

        //    //int[] assetCounts = { 0, 0, 0, 0 };

        //    foreach (string assetPath in assetKvp.Value.assets)
        //    {
        //        string fileExtension = assetPath.Substring(assetPath.LastIndexOf(".") + 1).ToLower();

        //        if (CheckStringInArray(fileExtension, OBJECT_EXTENSIONS))
        //        {
        //            assetKvp.Value.assetCounts[0]++;

        //            // Should only have one object per bundle, but support multiple for compatibility
        //            assetKvp.Value.JSONData.Add(GenerateJSONData(assetPath));
        //            Debug.Log("JSONData[" + (assetKvp.Value.JSONData.Count - 1) + "]: " + assetKvp.Value.JSONData[assetKvp.Value.JSONData.Count - 1]);
        //        }
        //        else if (CheckStringInArray(fileExtension, MATERIAL_EXTENSIONS))
        //        {
        //            assetKvp.Value.assetCounts[1]++;
        //        }
        //        else if (CheckStringInArray(fileExtension, TEXTURE_EXTENSIONS))
        //        {
        //            assetKvp.Value.assetCounts[2]++;
        //        }
        //        else
        //        {
        //            assetKvp.Value.assetCounts[3]++;
        //        }
        //    }
        //}
    }

    void OnGUI()
    {
        GUILayout.Label("Furniture JSON File Builder", EditorStyles.boldLabel);

        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Build JSON File", GUILayout.Width(150)))
        {
            CheckSettingsAndBuild();
        }

        if (GUILayout.Button("Refresh Data", GUILayout.Width(100)))
        {
            SetupData(true);
        }

        EditorGUILayout.EndHorizontal();

        float originalLabelWidth = EditorGUIUtility.labelWidth;

        EditorGUI.BeginChangeCheck();

        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

        GUILayout.Label("Marker Data Setup", EditorStyles.boldLabel);

        if (ListOfMarkerNames.Count > 0)
        {
            MarkerObject = (ImageTargetBehaviour)EditorGUILayout.ObjectField("Marker Object", MarkerObject, typeof(ImageTargetBehaviour), true);
            SelectedMarkerIndex = EditorGUILayout.Popup("Selected Marker:", SelectedMarkerIndex, ListOfMarkerNames.ToArray());

            for (int i = 0; i < 10; i++)
            {
                EditorGUILayout.BeginHorizontal();
                GUILayout.Space(20.0f);
                EditorGUILayout.LabelField("Child Object " + (i + 1).ToString() + ": ", GUILayout.MaxWidth(100.0f));
                //EditorGUILayout.TextField("", GUILayout.MaxWidth(200.0f));
                ListOfChildObjectNames[i] = GUILayout.TextField(ListOfChildObjectNames[i], 40, GUILayout.MaxWidth(300.0f));
                EditorGUILayout.EndHorizontal();
            }
        }

        //GUILayout.Label("Build Asset Bundle Options", EditorStyles.boldLabel);
        //EditorGUIUtility.labelWidth = 200.0f;
        //for (int i = 0; i < BUILD_ASSET_BUNDLE_OPTION_NAMES.Length; i++)
        //{
        //    BuildAssetBundleOptions[i] = EditorGUILayout.Toggle(BUILD_ASSET_BUNDLE_OPTION_NAMES[i], BuildAssetBundleOptions[i], GUILayout.MaxWidth(220.0f));
        //}


        //GUILayout.Label("Build Target Platforms", EditorStyles.boldLabel);
        //EditorGUIUtility.labelWidth = 60.0f;
        //for (int i = 0; i < BUILD_ASSET_TARGET_PLATFORM_NAMES.Length; i++)
        //{
        //    BuildAssetTargetPlatforms[i] = EditorGUILayout.Toggle(BUILD_ASSET_TARGET_PLATFORM_NAMES[i], BuildAssetTargetPlatforms[i], GUILayout.MaxWidth(80.0f));
        //}

        //GUILayout.Label("Select Asset Bundles to Build", EditorStyles.boldLabel);
        //EditorGUILayout.BeginHorizontal();
        //GUILayout.Label("Asset Bundle", EditorStyles.helpBox, GUILayout.Width(220.0f));
        //GUILayout.Label("# of objects", EditorStyles.helpBox, GUILayout.Width(80.0f));
        //GUILayout.Label("# of materials", EditorStyles.helpBox, GUILayout.Width(80.0f));
        //GUILayout.Label("# of textures", EditorStyles.helpBox, GUILayout.Width(80.0f));
        //GUILayout.Label("# of others", EditorStyles.helpBox, GUILayout.Width(80.0f));
        //EditorGUILayout.EndHorizontal();

        //EditorGUIUtility.labelWidth = 200.0f;
        //ListOfAssetsToBuild = new List<AssetBundlesProperties>();

        ////foreach (KeyValuePair<string, List<string>> assetKvp in DictOfAssets)
        //foreach (KeyValuePair<string, AssetBundlesProperties> assetKvp in DictOfAssets)
        //{
        //    EditorGUILayout.BeginHorizontal();

        //    assetKvp.Value.isBuildAssetBundle = EditorGUILayout.Toggle(assetKvp.Key, assetKvp.Value.isBuildAssetBundle, GUILayout.MaxWidth(220.0f));

        //    //int[] assetCounts = { 0, 0, 0, 0 };

        //    //foreach(string assetPath in assetKvp.Value.assets)
        //    //{
        //    //    string fileExtension = assetPath.Substring(assetPath.LastIndexOf(".") + 1).ToLower();

        //    //    if (CheckStringInArray(fileExtension, OBJECT_EXTENSIONS))
        //    //    {
        //    //        assetCounts[0]++;

        //    //        // Should only have one object per bundle, but support multiple for compatibility
        //    //        assetKvp.Value.JSONData.Add(GenerateJSONData(assetPath));
        //    //        Debug.Log("JSONData[" + (assetKvp.Value.JSONData.Count - 1) + "]: " + assetKvp.Value.JSONData[assetKvp.Value.JSONData.Count - 1]);
        //    //    }
        //    //    else if (CheckStringInArray(fileExtension, MATERIAL_EXTENSIONS))
        //    //    {
        //    //        assetCounts[1]++;
        //    //    }
        //    //    else if (CheckStringInArray(fileExtension, TEXTURE_EXTENSIONS))
        //    //    {
        //    //        assetCounts[2]++;
        //    //    }
        //    //    else
        //    //    {
        //    //        assetCounts[3]++;
        //    //    }
        //    //}

        //    using (new EditorGUI.DisabledScope(true))
        //    {
        //        //for(int i = 0; i < assetCounts.Length; i++)
        //        for (int i = 0; i < assetKvp.Value.assetCounts.Count; i++)
        //        {
        //            //EditorGUILayout.IntField(assetCounts[i], GUILayout.MaxWidth(80.0f));
        //            EditorGUILayout.IntField(assetKvp.Value.assetCounts[i], GUILayout.MaxWidth(80.0f));
        //        }
        //    }

        //    if (assetKvp.Value.isBuildAssetBundle)
        //    {
        //        ListOfAssetsToBuild.Add(assetKvp.Value);
        //    }

        //    if (GUILayout.Button("List files", GUILayout.MaxWidth(80.0f)))
        //    {
        //        AssetBundlesListing.Init(assetKvp.Value);
        //    }

        //    //EditorGUI.EndDisabledGroup();

        //    EditorGUILayout.EndHorizontal();
        //}

        if (GUILayout.Button("Close", GUILayout.MaxWidth(80.0f)))
        {
            SaveSettings();
            Close();
        }

        EditorGUILayout.EndScrollView();

        if (EditorGUI.EndChangeCheck())
        {
            SaveSettings();
        }

        EditorGUIUtility.labelWidth = originalLabelWidth;
    }

    private static bool CheckStringInArray(string inputString, string[] checkArray)
    {
        foreach (string checkString in checkArray)
        {
            if (inputString.Equals(checkString))
            {
                return true;
            }
        }

        return false;
    }

    private static void CheckSettingsAndBuild()
    {
        if (MarkerObject == null)
        {
            Debug.LogError("No ImageTarget object selected.");
            return;
        }

        //bool anyFound = false;
        //foreach (string childObjectName in ListOfChildObjectNames)
        //{
        //    if (!string.IsNullOrEmpty(childObjectName))
        //    {
        //        anyFound = true;
        //        break;
        //    }
        //}

        //if (!anyFound)
        //{
        //    Debug.LogError("No child objects entered for JSON, nothing was generated.");
        //    return;
        //}

        string finalJSON = GenerateJSONData();

        if (string.IsNullOrEmpty(finalJSON))
        {
            Debug.LogError("No child objects entered for JSON, nothing was generated.");
            return;
        }

        BuildScript.WriteJSONForAssetBundles(finalJSON, ListOfMarkerNames[SelectedMarkerIndex]);
        //string finalJSON = CombineJSONData(markerJSONData);

        //List<AssetBundleBuild> buildList = new List<AssetBundleBuild>();
        //foreach (AssetBundlesProperties assetBundlesProperties in ListOfAssetsToBuild)
        //{
        //    AssetBundleBuild assetBundleBuild = new AssetBundleBuild();
        //    assetBundleBuild.assetBundleName = assetBundlesProperties.assetBundleName;
        //    assetBundleBuild.assetNames = assetBundlesProperties.assets.ToArray();
        //    buildList.Add(assetBundleBuild);

        //    // Write JSON to file first
        //    if (assetBundlesProperties.JSONData.Count >= 1)
        //    {
        //        string finalJSON = CombineJSONData(assetBundlesProperties.JSONData);
        //        //Debug.Log("finalJSON: " + finalJSON);
        //        //IList abc = (IList) MiniJSON.Json.Deserialize(finalJSON);
        //        //Debug.Log("test final json: " + abc[0]);
        //        BuildScript.WriteJSONForAssetBundles(finalJSON, assetBundlesProperties.assetBundleName);
        //    }
        //}

        //if (BuildAssetTargetPlatforms[0])
        //{
        //    Debug.Log("Building " + buildList.Count + " assets for: " + BUILD_ASSET_TARGET_PLATFORM_NAMES[0]);
        //    //BuildScriptV2.BuildAllAssetBundlesTargetPlatform(babo, BuildTarget.Android, BUILD_ASSET_TARGET_PLATFORM_NAMES[0]);
        //    BuildScript.BuildAssetBundlesTargetPlatform(buildList, babo, BuildTarget.Android, BUILD_ASSET_TARGET_PLATFORM_NAMES[0]);
        //    //hasBuildTarget = true;
        //}

        //if (BuildAssetTargetPlatforms[1])
        //{
        //    Debug.Log("Building " + buildList.Count + " assets for: " + BUILD_ASSET_TARGET_PLATFORM_NAMES[1]);
        //    //BuildScriptV2.BuildAllAssetBundlesTargetPlatform(babo, BuildTarget.iOS, BUILD_ASSET_TARGET_PLATFORM_NAMES[1]);
        //    BuildScript.BuildAssetBundlesTargetPlatform(buildList, babo, BuildTarget.Android, BUILD_ASSET_TARGET_PLATFORM_NAMES[1]);
        //    //hasBuildTarget = true;
        //}

        //			if(buildAssetTargetPlatforms[2])
        //			{
        //				Debug.Log ("Building assets for: " + buildAssetTargetPlatformNames[2] + ", version: " + versionNumber);
        //				BuildScript.BuildAssetBundlesTargetPlatform(babo, buildAssetTargetPlatformNames[2], versionNumber);
        //				hasBuildTarget = true;
        //			}
        //
        //			if(buildAssetTargetPlatforms[3])
        //			{
        //				Debug.Log ("Building assets for: " + buildAssetTargetPlatformNames[3] + ", version: " + versionNumber);
        //				BuildScript.BuildAssetBundlesTargetPlatform(babo, buildAssetTargetPlatformNames[3], versionNumber);
        //				hasBuildTarget = true;
        //			}

        //if (!hasBuildTarget)
        //{
        //    Debug.LogError("No build targets selected, assets not generated");
        //}
    }

    /// <summary>
    /// Raises the destroy event. Saves the bundle options to editor preferences to load next time
    /// </summary>
    public void OnDestroy()
    {
        SaveSettings();
    }

    private static void SaveSettings()
    {
        //			EditorPrefs.SetString ("versionNumber", versionNumber);

        //for (int i = 0; i < BUILD_ASSET_BUNDLE_OPTION_NAMES.Length; i++)
        //{
        //    EditorPrefs.SetBool(BUILD_ASSET_BUNDLE_OPTION_NAMES[i], BuildAssetBundleOptions[i]);
        //}

        //for (int i = 0; i < BUILD_ASSET_TARGET_PLATFORM_NAMES.Length; i++)
        //{
        //    EditorPrefs.SetBool(BUILD_ASSET_TARGET_PLATFORM_NAMES[i], BuildAssetTargetPlatforms[i]);
        //}
    }

    private static string GenerateJSONData()
    {
        bool isAnyChildObject = false;

        IDictionary<string, object> objectDictionaryForJSON = new Dictionary<string, object>();
        objectDictionaryForJSON.Add("MarkerName", ListOfMarkerNames[SelectedMarkerIndex]);
        objectDictionaryForJSON.Add("Version", 1);

        IList<float> insertList = new List<float>();
        insertList.Add(MarkerObject.transform.localPosition.x);
        insertList.Add(MarkerObject.transform.localPosition.y);
        insertList.Add(MarkerObject.transform.localPosition.z);
//        objectDictionaryForJSON.Add("Position", MiniJSON.Json.Serialize(insertList));
		objectDictionaryForJSON.Add("Position", insertList);

        insertList.Clear();
        insertList.Add(MarkerObject.transform.localEulerAngles.x);
        insertList.Add(MarkerObject.transform.localEulerAngles.y);
        insertList.Add(MarkerObject.transform.localEulerAngles.z);
//        objectDictionaryForJSON.Add("Rotation", MiniJSON.Json.Serialize(insertList));
		objectDictionaryForJSON.Add("Rotation", insertList);

        insertList.Clear();
        insertList.Add(MarkerObject.transform.localScale.x);
        insertList.Add(MarkerObject.transform.localScale.y);
        insertList.Add(MarkerObject.transform.localScale.z);
//        objectDictionaryForJSON.Add("Scaling", MiniJSON.Json.Serialize(insertList));
		objectDictionaryForJSON.Add("Scaling", insertList);

        IList<IDictionary<string, string>> listOfObjectsList = new List<IDictionary<string, string>>();
        foreach (string childObjectName in ListOfChildObjectNames)
        {
            if (!string.IsNullOrEmpty(childObjectName))
            {
                if (!isAnyChildObject)
                {
                    isAnyChildObject = true;
                }

                IDictionary<string, string> objectNameDictionary = new Dictionary<string, string>();
                objectNameDictionary.Add("ObjectName", childObjectName);
                listOfObjectsList.Add(objectNameDictionary);
            }
        }
//        objectDictionaryForJSON.Add("ListOfObjects", MiniJSON.Json.Serialize(listOfObjectsList));
		objectDictionaryForJSON.Add("ListOfObjects", listOfObjectsList);

        if (!isAnyChildObject)
        {
            return "";
        }

        return MiniJSON.Json.Serialize(objectDictionaryForJSON);
    }

    private static string CombineJSONData(List<string> JSONData)
    {
        IList<string> JSONDataList = new List<string>();

        foreach (string JSONDataEntry in JSONData)
        {
            JSONDataList.Add(JSONDataEntry);
        }

        return MiniJSON.Json.Serialize(JSONDataList);
    }

    //[MenuItem("Assets/AssetBundles/Test/Names")]
    //static void GetNames()
    //{
    //    string[] names = AssetDatabase.GetAllAssetBundleNames();
    //    foreach (string name in names)
    //    {
    //        string[] assetPaths = AssetDatabase.GetAssetPathsFromAssetBundle(name);
    //        Debug.Log("AssetBundle: " + name);

    //        foreach (string assetPath in assetPaths)
    //        {
    //            Debug.Log("AssetPath: " + assetPath);
    //        }
    //    }
    //}

    //[MenuItem("Assets/AssetBundles/Test/JSON")]
    //static void GenerateJSONFileTest()
    //{
    //    string b = "Assets/UIDesignMaterial/ProductCategory/ListofProductImage.prefab";

    //    //int startIndex = assetPath.LastIndexOf("\\");
    //    //int endIndex = assetPath.LastIndexOf(".");
    //    //int length = endIndex - startIndex;

    //    //string objectName = assetPath.Substring(startIndex, length);
    //    //GameObject.Find(objectName);

    //    IDictionary<string, object> dic = new Dictionary<string, object>();

    //    GameObject a = AssetDatabase.LoadAssetAtPath(b, typeof(GameObject)) as GameObject;
    //    Debug.Log("a: " + a.name);

    //    IList<float> pos = new List<float>();

    //    pos.Add(a.transform.localPosition.x);
    //    pos.Add(a.transform.localPosition.y);
    //    pos.Add(a.transform.localPosition.z);

    //    string c = MiniJSON.Json.Serialize(pos);
    //    Debug.Log("c: " + c);

    //    dic.Add("Position", c);

    //    string d = MiniJSON.Json.Serialize(dic);
    //    Debug.Log("d: " + d);
    //}
}
#endif