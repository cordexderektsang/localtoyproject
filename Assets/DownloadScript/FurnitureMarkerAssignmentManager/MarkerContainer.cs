﻿using System.Collections.Generic;
using System.Xml.Serialization;

[XmlRoot("QCARConfig")]
public class MarkerContainer
{
    [XmlArray("Tracking")]
    [XmlArrayItem("ImageTarget")]
    public List<Marker> markers = new List<Marker>();
}