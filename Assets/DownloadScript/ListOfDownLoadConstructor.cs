﻿using UnityEngine;
using System.Collections.Generic;

public class ListOfDownLoadConstructor{}
public class UIAtlasDownLoadDataConstructor
{

    public string altas_Name;
    public string Bundle_Name;


    public string _Bundle_Name
    {
        get { return Bundle_Name; }
        set { Bundle_Name = value; }
    }

    public string _altas_Name
    {
        get { return altas_Name; }
        set { altas_Name = value; }
    }


    public void setAllAltasData(string _altas_Name , string _Bundle_Name)
    {
        altas_Name = _altas_Name;
        Bundle_Name = _Bundle_Name;
    }
}

public class DownLoadListConstructor
{

    public string file_Name;
    public string type;
    public string version;


    public string _file_Name
    {
        get { return file_Name; }
        set { file_Name = value; }
    }

    public string _type
    {
        get { return type; }
        set { type = value; }
    }

    public string _version
    {
        get { return version; }
        set { version = value; }
    }


    public void setAllDownloadData(string _file_Name, string _type , string _version)
    {
        file_Name = _file_Name;
        type = _type;
        version = _version;
    }
}


