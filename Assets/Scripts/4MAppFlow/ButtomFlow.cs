﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using CCARPlatformFramework.CCConstants;
using CCARPlatformFramework.CCDataHandling;
using CCARPlatformFramework.CCTools;

public class ButtomFlow : MonoBehaviour
{


	#region public

	public GameObject settingPopUpObject;
	public GameObject languagePopUpObject;
	public GameObject noticeBroadPopUpObject;
	public GameObject webNoticeObject;
	public GameObject loadingObject;
	public GameObject frontObject;
	public GameObject playButtonObject;
	public Transform groundVolumePoint;
	public Transform volumePoint;
	public Transform backgroundMuteUI;
	public Transform volumeMuteUI;
	public bool isLocal = true;
	public ScaleScroller scaleScroller;
	public LoadJson loadJson;
	public UISprite backGroundAudioSprite;
	public UISprite volumeAudioSprite;
	private InputTouch inputTouch;


	#endregion

	#region private

	private const string AlertOnUiName = "UI_setting_music@4x";
	private const string AlertMuteUiName = "UI_setting_music_mute@4x";
	private const string VolumeOnUiName = "UI_setting_sound@4x";
	private const string VolumeMuteUiName = "UI_setting_sound_mute@4x";
	//private const string AlertOnUiName = "UI_setting_sound@4x";
	//private const string AlertMuteUiName = "UI_setting_sound_mute@4x";
	private const string VolumePointOnUiName = "UI_setting_increase@4x";
	private const string VolumePointOffUiName = "UI_setting_decrease@4x";
	public const string LanguageUIUnSelectedName = "btn_language_nonSelect@4x";
	public const string LanguageUISelectedName = "btn_language_Selected@4x";
	//private const string UILabel_LanguageName = "UILabel_LanguageName";

	private const string UISprite_SelectLanguage = "UISprite_SelectLanguage";
	private const string webUrlFor4M = "https://www.google.com.hk/";
	private const string scene = "Unicorn";
	private const string UIDownLoadButton = "btn_download@4x";
	private const string UIPlayButton = "btn_play@4x";

	public const string NoMute = "none";
	public const string Mute = "mute";

	public int backGroundVolumeIndex = -1;
	public int volumeIndex = -1;
	public string isMuteForVolume = "none";
	public string isMuteForBackGround = "none";
	public static bool isGameReady = false;

	#endregion

	#region Setup Audio 
	public void Start ()
	{
		backGroundVolumeIndex = CoreAudioSetting.BackGroundConvertToVolumePoint;
		volumeIndex = CoreAudioSetting.VolumeValueConvertToVolumePoint;
		isMuteForVolume = CoreAudioSetting.MuteValueForVolume;
		isMuteForBackGround = CoreAudioSetting.MuteValueForBackGround;
		inputTouch = GetComponent<InputTouch>();
		if (volumePoint != null) {
			SetUpVolumePoint ();
			SetUpBackGroundVolumePoint ();
			SetupVolumeMute ();
			SetupBackGroundMute ();
		}
		CoreAudioSetting.SetUpAllAudio ();
	}
	#endregion

	#region button

	public void SetupVolumeMute ()
	{
		if (isMuteForVolume.Equals(Mute)) {
			volumeMuteUI.GetComponent<UISprite> ().spriteName = VolumeMuteUiName;
			CoreAudioSetting.OnOffAllSound ();
		} else {
			volumeMuteUI.GetComponent<UISprite> ().spriteName = VolumeOnUiName;

		}


	}

	public void SetupBackGroundMute ()
	{

		if (isMuteForBackGround.Equals(Mute)) {
			backgroundMuteUI.GetComponent<UISprite> ().spriteName = AlertMuteUiName;
			CoreAudioSetting.OnOffAllSound ();

		} else {
			backgroundMuteUI.GetComponent<UISprite> ().spriteName = AlertOnUiName;

		}
	}

	public void SetUpVolumePoint ()
	{
		if (backGroundVolumeIndex >= 0) {
			for (int i = 0; i < volumePoint.childCount; i++) {
				if (i < backGroundVolumeIndex) {
					volumePoint.GetChild (i).gameObject.GetComponent<UISprite> ().spriteName = VolumePointOnUiName;
				} else {
					volumePoint.GetChild (i).gameObject.GetComponent<UISprite> ().spriteName = VolumePointOffUiName;
				}
			} 

		}

	}

	public void SetUpBackGroundVolumePoint ()
	{
		if (volumeIndex >= 0) {
			for (int i = 0; i < groundVolumePoint.childCount; i++) {
				if (i < volumeIndex) {
					groundVolumePoint.GetChild (i).gameObject.GetComponent<UISprite> ().spriteName = VolumePointOnUiName;
				} else {
					groundVolumePoint.GetChild (i).gameObject.GetComponent<UISprite> ().spriteName = VolumePointOffUiName;
				}
			} 

		}

	}


	public void OnOffVolume (/*UISprite target, UIButton targetButton*/)
	{
		if (volumeAudioSprite.spriteName.Equals (VolumeMuteUiName)) {
			volumeAudioSprite.spriteName = VolumeOnUiName;
			//targetButton.normalSprite = VolumeOnUiName;
			CoreAudioSetting.MuteValueForVolume = "none";
			CoreAudioSetting.OnOffAllSound ();
		
		} else {
			volumeAudioSprite.spriteName = VolumeMuteUiName;
			//targetButton.normalSprite = VolumeMuteUiName;
			CoreAudioSetting.MuteValueForVolume = "mute";
			CoreAudioSetting.OnOffAllSound ();
	
		}

	}

	public void OnOffAlert (/*UISprite target, UIButton targetButton*/)
	{
		if (backGroundAudioSprite.spriteName.Equals (AlertMuteUiName)) {
			backGroundAudioSprite.spriteName = AlertOnUiName;
			//targetButton.normalSprite = AlertOnUiName;
			CoreAudioSetting.MuteValueForBackGround = "none";
			CoreAudioSetting.OnOffAllSound ();

		} else {
			;
			backGroundAudioSprite.spriteName = AlertMuteUiName;
			//targetButton.normalSprite = AlertMuteUiName;
			CoreAudioSetting.MuteValueForBackGround = "mute";
			CoreAudioSetting.OnOffAllSound ();
		}
	}
		


	public void IncreaseBackgroundMusicPoint (Transform target)
	{
		if (backGroundVolumeIndex < target.childCount ) {
			backGroundVolumeIndex++;
			SetUpVolumePoint ();
			if (backGroundVolumeIndex == 1) {
				CoreAudioSetting.OnBGMClick (true, false);
				CoreAudioSetting.MuteValueForBackGround = "none";
				CoreAudioSetting.OnOffAllSound ();
				backGroundAudioSprite.spriteName = AlertOnUiName;
			}
		
		}
	}
		
	public void DecreaseBackgroundMusicPoint (Transform target)
	{
		if (backGroundVolumeIndex > 0) {
			backGroundVolumeIndex--;
			SetUpVolumePoint ();
			CoreAudioSetting.OnBGMClick (false, false);
			if (backGroundVolumeIndex == 0) {
				backGroundAudioSprite.spriteName = AlertMuteUiName;
				CoreAudioSetting.MuteValueForBackGround = "mute";
				CoreAudioSetting.OnOffAllSound ();

			}
		}
	
	}

	public void IncreaseVolumePoint (Transform target)
	{
		if (volumeIndex < target.childCount) {
			volumeIndex++;
			SetUpBackGroundVolumePoint ();
			CoreAudioSetting.OnBGMClick (true,true);
			if (volumeIndex == 1) {
				volumeAudioSprite.spriteName = VolumeOnUiName;
				CoreAudioSetting.MuteValueForVolume = "none";
				CoreAudioSetting.OnOffAllSound ();
				inputTouch.PlayClickSound();
			}
		}
	}

	public void DecreaseVolumePoint (Transform target)
	{

		if (volumeIndex > 0) {
			volumeIndex--;
			SetUpBackGroundVolumePoint ();
			CoreAudioSetting.OnBGMClick (false, true);
			if (volumeIndex == 0) {
				volumeAudioSprite.spriteName = VolumeMuteUiName;
				CoreAudioSetting.MuteValueForVolume = "mute";
				CoreAudioSetting.OnOffAllSound ();
			}
		}
	}

	public void OnClickLanguageSelection (UISprite target, Transform parentToLoopChildForReset)
	{
		string currentLabel = CoreLanguage.DefaultLanguage;
		foreach (Transform parentChild in parentToLoopChildForReset) {
	
			//string Language = parentChild.Find (UILabel_LanguageName).GetComponent<UILabel> ().text;
			Transform GetUISprite = parentChild.Find (UISprite_SelectLanguage);
			UISprite UIsprite = GetUISprite.GetComponent<UISprite> ();
		
			UIsprite.spriteName = LanguageUIUnSelectedName;


		}

		target.spriteName = LanguageUISelectedName;
		currentLabel = target.transform.parent.GetComponent<UITexture> ().mainTexture.name;
		Debug.Log (currentLabel);
		CoreLanguage.LanguagePref = currentLabel;
		CoreLanguage.SearchForLanguagePack ();

	}



	public void OnClickCallForSettingPopUpFunction ()
	{
		settingPopUpObject.SetActive (true);

	}

	public void OnClickCallForLanguagePopUpFunction ()
	{

		languagePopUpObject.SetActive (true);
	}

	public void OnClickCallForSettingCloseFunction ()
	{
		settingPopUpObject.SetActive (false);

	}

	public void OnClickCallForLanguageCloseFunction ()
	{

		languagePopUpObject.SetActive (false);
	}

	public void OnClickCallNoticeBroadCloseFunction ()
	{

		noticeBroadPopUpObject.SetActive (false);
	}

	public void OnClickCallForNoticeBroadPopUpeFunction ()
	{

		noticeBroadPopUpObject.SetActive (true);
	}

	public void OnClickCallWebBroadCloseFunction ()
	{

		webNoticeObject.SetActive (false);
	}

	public void OnClickCallForWebBroadPopUpFunction ()
	{

		webNoticeObject.SetActive (true);
	}

	public void OnClickCallForWebBrowserFunction ()
	{
		OnClickCallWebBroadCloseFunction ();
		Application.OpenURL (webUrlFor4M);

	}

	public void OnClickCallForLoading ()
	{
		frontObject.SetActive (false);
		loadingObject.SetActive (true);
		StartCoroutine (SceneChange ());
	}

	public IEnumerator SceneChange ()
	{
		isGameReady = false;
		yield return new WaitForSeconds (1);
		//LoadSceneFromBundle ();
		SceneManager.LoadScene (scene);
	}


	public void OnClickGamePlay (UIButton button)
	{
		isGameReady = false;
		string name = button.gameObject.name;
		GlobalStaticVariable.CurrentOnClickSceneName = name;
		GlobalStaticVariable.currentIndex = scaleScroller.GetCurrentIndex ();
		//loadJson.LoadSceneMarkerAndObjectJson ();
		Debug.Log ("=============== start Menu Down For " + name + " ================");
		//StartCoroutine (WaitForDownload ());
		if (isLocal) {
			playButtonObject.SetActive (true);
			button.enabled = false;
			GlobalStaticVariable.currentIndex = scaleScroller.GetCurrentIndex ();
			StartCoroutine (WaitForDownloadlocal ());
		}
	}


	public IEnumerator WaitForDownloadlocal ()
	{

			yield return new WaitForSeconds (1);
			playButtonObject.GetComponent<UISprite> ().name = UIPlayButton;
			playButtonObject.GetComponent<UIButton> ().normalSprite = UIPlayButton;
			playButtonObject.GetComponent<UIButton> ().enabled = true;
			isGameReady = true;
	
	}

	public IEnumerator WaitForDownload ()
	{
		while(true){

			yield return new WaitForSeconds (1f);
			if (GlobalStaticVariable.finishDownload) {
				Debug.Log ("Finish");
				playButtonObject.GetComponent<UISprite> ().name = UIPlayButton;
				playButtonObject.GetComponent<UIButton> ().normalSprite = UIPlayButton;
				playButtonObject.GetComponent<UIButton> ().enabled = true;
				playButtonObject.SetActive (true);
				isGameReady = true;
				break;
			} 

		}


	}

	public void LoadSceneFromBundle()
	{
		foreach(SceneJsonData data in LoadJson.LocalSceneJsonDataReference)
		{
			string path = data.ScenePath;
			CCAssetBundleManager.LoadSceneFromBundle (path,data.SceneFileName,data.SceneName);
		}
	}


	#endregion
}
