﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SetupNumberOfLanguage : MonoBehaviour {

	//public int numberOfLanguage =4;
	public ToyCoreDownLoadScript toyCoreDownLoadScript;
	public GameObject languageBar;
	public Transform langageWrapper;
	public List<UILabel> allLabel = new List<UILabel> ();
	public ButtomFlow buttomFlowScript;
	public bool isLocal = true;
	private const string UISprite_SelectLanguage ="UISprite_SelectLanguage";
	//private const string UILabel_LanguageName ="UILabel_LanguageName";
	private const string UISprite_DownloadButton ="UISprite_DownloadButton";

	// Use this for initialization
	//set font ,label// json set font for each language



	void Awake (){
		if (!isLocal) {
			LoadJson.languageFunction += onCallSetUpNumberOfLanguage;
		} 
	}


	void onCallSetUpNumberOfLanguage () {
		Debug.Log ("onCallSetUpNumberOfLanguage Function");
		foreach (LanguageJsonData data in  LoadJson.LocalLanguageJsonDataReference) {
			GameObject target = Instantiate (languageBar);
			CoreNguiButtonAddAndRemove.AddParameterFunction (buttomFlowScript, new object[] {
				target.transform.FindChild (UISprite_SelectLanguage).GetComponent<UISprite> (),
				langageWrapper
			}, target.GetComponent<UIButton> (), "OnClickLanguageSelection");

			target.transform.parent = langageWrapper.transform;
			target.name = languageBar.name;
			target.transform.localScale = new Vector3 (1, 1, 1);
			target.transform.localPosition = Vector3.zero;
			target.transform.localEulerAngles = Vector3.zero;
			UITexture texture = target.GetComponent<UITexture> ();

			if (data.isLocal)  {
				Debug.Log ("Language Name : " +data.languageName + " : isLocal : " + data.isLocal);
				texture.mainTexture = Resources.Load (data.languageName) as Texture2D;
			} else {
				string DirectionaryPath = data.texturePath + data.textureFileName;
				Debug.Log ("Language Name : " +data.languageName + " : isLocal : " + data.isLocal + " Path " + DirectionaryPath);
				texture.mainTexture = SetupNumberOfScrollObject.LoadPNG(DirectionaryPath);
			}

			if (!data.isLocal) {
				string BundlePathName = Application.persistentDataPath +  ToyCoreDownLoadScript.LanguageFolder  + "/"+ data.fontPackageName +GlobalStaticVariable.AssetBundle_EXT;
				string SumExtPathName = Application.persistentDataPath +  ToyCoreDownLoadScript.LanguageFolder  + "/"+ data.fontPackageName +GlobalStaticVariable.AssetBundle_EXT +GlobalStaticVariable.CHECKSUM_EXT;
				UIButton downloadButton = target.transform.FindChild (UISprite_DownloadButton).GetComponent<UIButton> ();
				if (!File.Exists (BundlePathName) || !File.Exists (SumExtPathName)) {
					CoreNguiButtonAddAndRemove.AddParameterFunction (toyCoreDownLoadScript, new object[] {
						data.fontPackageName.ToLower ()
					}, downloadButton, "DownloadLanguagePack");
					downloadButton.transform.gameObject.SetActive (true);
				} else {
					downloadButton.transform.gameObject.SetActive (false);
				}
			}
			if (data.languageName.Equals (CoreLanguage.LanguagePref)) {
				Debug.Log ("Default lanuage set to " + CoreLanguage.LanguagePref);
				target.transform.FindChild (UISprite_SelectLanguage).GetComponent<UISprite> ().spriteName = ButtomFlow.LanguageUISelectedName;

			} else {
				target.transform.FindChild (UISprite_SelectLanguage).GetComponent<UISprite> ().spriteName = ButtomFlow.LanguageUIUnSelectedName;
			}
			langageWrapper.GetComponent<UIGrid> ().enabled = true;
		}
		
	}

	void OnDestroy() 
	{
		LoadJson.languageFunction -= onCallSetUpNumberOfLanguage ;
	}
	
	// Update is called once per frame
}
