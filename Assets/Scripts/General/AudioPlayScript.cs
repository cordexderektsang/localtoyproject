﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayScript : MonoBehaviour {

	private AudioListController audioListController;
	public bool IsPlaying = false;
	private AudioSource thisAudioSource;

	public void Awake ()
	{

		audioListController = GameObject.Find ("AudioSourceClickEffect").GetComponent<AudioListController> ();
		thisAudioSource = GetComponent<AudioSource> ();
		SetupVolumeMute ();
		SetupBackGroundMute ();
		float backGroundValue = 	((float)CoreAudioSetting.BackGroundValue) / ((float) 100);
		float volumeValue = 	((float)CoreAudioSetting.VolumeValue) / ((float) 100);
		if (this.gameObject.tag.Equals (AudioListController.BackGround)) {
			thisAudioSource.volume = backGroundValue;
		} else {
			thisAudioSource.volume = volumeValue;
		}

		thisAudioSource.loop = true;
	}


	public void SetupVolumeMute ()
	{

		if (CoreAudioSetting.MuteValueForVolume.Equals(ButtomFlow.Mute)) {

			CoreAudioSetting.OnOffAllSound ();
		} 


	}

	public void SetupBackGroundMute ()
	{

		if (CoreAudioSetting.MuteValueForBackGround.Equals(ButtomFlow.Mute)) {

			CoreAudioSetting.OnOffAllSound ();

		} 
	}

	public void GetAudio (string name, string status)
	{
		Debug.Log (audioListController);
		List<AudioClip> sounds = audioListController.GetGroupAudioClip (name);
		//int ran = Random.Range (0, sounds.Count);
		AudioClip sound =ReturnStatusAudio(sounds,status);
		if (sound != null) {
			IsPlaying = true;
			thisAudioSource.PlayOneShot (sound);
			StartCoroutine ("countTime", sound.length);
		}
	}
		

	private AudioClip ReturnStatusAudio ( List<AudioClip> sounds , string status)
	{

		foreach (AudioClip data in sounds){
			Debug.Log (data.name);
			if (data.name.Contains (status)) {
		

				return data;


			}

		}
		Debug.Log ("Target audio not find");
		return null;
	}


	private IEnumerator countTime (float t)
	{
		yield return new WaitForSeconds (t);
		IsPlaying = false;
	}


	public void stopAll ()
	{
		if (thisAudioSource != null) {
			Debug.Log ("stopAll");
			StopCoroutine ("countTime");
			thisAudioSource.enabled = false;
			thisAudioSource.enabled = true;
			IsPlaying = false;
		}
	}
}
