﻿using UnityEngine;
using System.Collections;
using Vuforia;
public class AutoFocus : MonoBehaviour {

	private float maxTIme = 40; 
	public float countTime = 0;
	private bool isAuto = true;
	private bool mVuforiaStarted = false;
		

	void Start () 
	{

		VuforiaARController vuforia = VuforiaARController.Instance;
		vuforia.RegisterVuforiaStartedCallback(StartAfterVuforia);

	}

	private void StartAfterVuforia()
	{
		mVuforiaStarted = true;
		//SetAutofocus();
	}

	// Use this for initialization
	// Update is called once per frame
	void Update () 
	{
		//SetAutofocus();
		/*if (countTime < maxTIme && !isAuto) {

			countTime += Time.deltaTime;

		} else {

			isAuto = true;
		}
		if (isAuto) 
		{
			Vuforia.CameraDevice.Instance.SetFocusMode (Vuforia.CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
			isAuto = false;
			countTime = 0;
		}*/
		if(Input.touchCount == 1 && GlobalStaticVariable.RayhitObject == null)
		{
			Vuforia.CameraDevice.Instance.SetFocusMode (Vuforia.CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
			//isAuto = false;
			//countTime = 0;

		}
	}

	private void SetAutofocus()
	{
		if (CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO))
		{
			Debug.Log("Autofocus set");
		}
		else
		{
			// never actually seen a device that doesn't support this, but just in case
			Debug.Log("this device doesn't support auto focus");
		}
	}
}
