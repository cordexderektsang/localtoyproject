﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetDeviceRegion : MonoBehaviour {
	public static string systemLanguage = "";
	// Use this for initialization

	public void Start ()
	{
		GetDeviceLanguage ();

	}
	public void GetDeviceLanguage () 
	{
		systemLanguage =  Application.systemLanguage.ToString ();
		if (string.IsNullOrEmpty (systemLanguage)) {
			systemLanguage = "English";
		}
		Debug.Log ("Detecting Device Language is : " + systemLanguage);
	}
	

}
