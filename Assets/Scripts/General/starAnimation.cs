﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class starAnimation : MonoBehaviour {
 	private Vector2 Direction;
	private float scaleSubstract = 0.005f;
	private bool isFinish = false;
	private int time = 2;
	private UITexture textureStar;
	private Coroutine starFunction;
	private Coroutine fadeFunction;
	private float counterForAlpha = 1;
	private int randomRotation;


	// Use this for initialization
	void Start () 
	{
		textureStar = GetComponent<UITexture> ();
		this.gameObject.transform.localScale = new Vector3 (0.2f, 0.2f, 0);
		Direction = ReturnRandomDirection ();
		starFunction = StartCoroutine (MoveStar());
		fadeFunction = StartCoroutine (FadeOut());
		randomRotation = Random.Range (0, 360);
		this.transform.localEulerAngles = new Vector3 (0,0,randomRotation);
	
	}
	
	public IEnumerator MoveStar ()
	{
		while (true) 
		{
			if (!GlobalStaticVariable.IsStarDiplay) {
				textureStar.enabled = false;
			} else {
				textureStar.enabled = true;
			}
			if (gameObject.transform.localScale.x > 0) 
			{
				yield return new WaitForSeconds (0.01f);
				this.gameObject.transform.localPosition += new Vector3 (Direction.x, Direction.y, 0);
				this.gameObject.transform.localScale -= new Vector3 (scaleSubstract, scaleSubstract, 0);

			} else {

				Destroy (this.gameObject, 0.01f);
				break;

			}
		}
	}


	public Vector2 ReturnRandomDirection ()
	{
		int random = Random.Range (0, 4);
		switch(random) 
		{
		case 0:
			return	Vector2.left*time;
			break;
		case 1:
			return	Vector2.right*time;
			break;
		case 2:
			return	Vector2.up*time;
			break;
		case 3:
			return	Vector2.down*time;
			break;
		default:
			return	Vector2.right*time;
			break;
		}

	}

	public IEnumerator FadeOut ()
	{

		while (true) 
		{
			yield return new WaitForSeconds (0.01f);
			if (!GlobalStaticVariable.IsStarDiplay) {
				textureStar.enabled = false;
			} else {
				textureStar.enabled = true;
			}

			if (counterForAlpha > 0) {

				counterForAlpha -= 0.02f;
			}
			if (textureStar != null) {
				textureStar.alpha = counterForAlpha;

			}
			if (counterForAlpha <= 0) {
				break;
			}
		
		}

	}
}
