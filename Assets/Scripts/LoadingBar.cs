﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingBar : MonoBehaviour {
	public Transform bodyObject;
	public UITexture  bodyTexture;	
	public GameObject endTexture;

	private int width; 


	// Use this for initialization
	void Start () {

		endTexture.gameObject.SetActive (false);
		width = bodyTexture.width;
		bodyTexture.width = 0;
		//width = bodyLoading
		
	}
	
	// Update is called once per frame
	public void LoadingBarProgress (int currentItem , int MaxItem)
	{
		int pecentage = width / MaxItem;

		bodyTexture.width = pecentage;

	}

	public void wwwProgress(float progress ,string url)
	{
		//Debug.Log (progress +" : " + url);

	}
}
