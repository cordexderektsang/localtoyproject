﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using CCARPlatformFramework.CCConstants;
using CCARPlatformFramework.CCDataHandling;
using CCARPlatformFramework.CCTools;

public class LoadJson : MonoBehaviour {
	
	private string menuSpecficFolder  = "Jsons";
	public const string listOfSceneAndUI  = "ListOfSceneAndUI" + GlobalStaticVariable.JSON_EXT;
	public const string languageList  = "LanguageList" + GlobalStaticVariable.JSON_EXT;
	public const string markerAndObjectList  = "MarkerAndObjectList";

	#region Local Json Array
	private bool isLocalMenuJsonLoaded = false;
	private bool isLocalLanguageJsonLoaded = false;
	public static bool isSuccessfulLoadedLocalJson = false;
	public static List<MenuJsonData> LocalMenuJsonDataReference  = new List<MenuJsonData>();
	public static List<LanguageJsonData> LocalLanguageJsonDataReference  = new List<LanguageJsonData>();
	public static List<MarkerJsonData> LocalMarkerJsonDataReference  = new List<MarkerJsonData>();
	public static List<ObjectJsonData> LocalObjectJsonDataReference  = new List<ObjectJsonData>();
	public static List<SceneJsonData> LocalSceneJsonDataReference  = new List<SceneJsonData>();
	public static List<AudioData> LocalAudioJsonDataReference  = new List<AudioData>();
	public static List<UIData> LocalUIJsonDataReference  = new List<UIData>();
	#endregion 

	#region Online Json Array
	private bool isOnlineMenuJsonLoaded = false;
	private bool isOnlineLanguageJsonLoaded = false;
	public static bool isSuccessfulLoadedOnlineJson = false;
	public  List<MenuJsonData> OnlineMenuJsonDataReference  = new List<MenuJsonData>();
	public  List<LanguageJsonData> OnlineLanguageJsonDataReference  = new List<LanguageJsonData>();
	public  List<MarkerJsonData> OnlineMarkerJsonDataReference  = new List<MarkerJsonData>();
	public  List<ObjectJsonData> OnlineObjectJsonDataReference  = new List<ObjectJsonData>();
	public  List<SceneJsonData> OnlineOSceneJsonDataReference  = new List<SceneJsonData>();
	public  List<AudioData> OnlineAudioJsonDataReference  = new List<AudioData>();
	public  List<UIData> OnlineUIJsonDataReference  = new List<UIData>();
	#endregion 


	public delegate void loadMenuFunction ();
	public static loadMenuFunction menuUIfunction;
	public delegate void loadLanguageFunction ();
	public static loadLanguageFunction languageFunction;

	public ToyCoreDownLoadScript toyCoreDownLoadScript;


	/*
	 *Check is any json on local 
	 * if yes - > load local json - > then get online json -> compare
	 * if not - > get online json 
	 * 
	 */
	public bool CheckAndStoreLocalJson ()
	{
		
		bool isExist = isLocalJsonExistForMenuAndLanguage ();
		if(isExist)
		{
			LoadAllLocalJson (isSuccessfulLoadedLocalJson);
		}
		return isExist;


	}

	#region Load Local Image 
	/// <summary>
	/// Switchs the case for load image.
	/// </summary>
	/// <param name="path">Path.</param>
	/// <param name="fileName">File name.</param>
	/// <param name="typeOfDownload">Type of download.</param>
	/// <param name="index">Index.</param>
	public void SwitchCaseForLoadImage(string path, string fileName , ToyCoreDownLoadScript.FileDownloadType typeOfDownload , int index)
	{
		switch (typeOfDownload) {
		case ToyCoreDownLoadScript.FileDownloadType.MenuPng:
			foreach (MenuJsonData data in LoadJson.LocalMenuJsonDataReference) {
				if (fileName.Equals (data.pictureName + GlobalStaticVariable.PNG)) {
					data.texturePath = path;
					data.textureFileName = fileName;
					Debug.Log ("assign Menu Picture Path to List array " + data.pictureName +" : "+  data.sceneName + " : " + data.texturePath + " : " + data.textureFileName );
				}
			}
				Debug.Log (index + " : " + LocalMenuJsonDataReference.Count);
			if (index == LocalMenuJsonDataReference.Count -1) {
				if (menuUIfunction != null) {
					menuUIfunction ();
				}
			}
	
			break;
		case ToyCoreDownLoadScript.FileDownloadType.LanguagePng:
			foreach (LanguageJsonData data in LoadJson.LocalLanguageJsonDataReference) {
				if (fileName.Equals (data.languageName + GlobalStaticVariable.PNG)) {
					data.texturePath = path;
					data.textureFileName = fileName;
					Debug.Log ("assign Language Picture Path to List array " +data.languageName + " : " + data.texturePath + " : " + data.textureFileName);
				}
			}
			if (index == LocalLanguageJsonDataReference.Count-1) {
				if (languageFunction != null) {
					languageFunction ();
				}
			}

			break;
		}

	}


	#endregion

	#region Load Online Json 





	public void ReadOnlineJsonForMenu (string jsonText)
	{

		string JSONText = jsonText;


		if (!string.IsNullOrEmpty(JSONText))
		{

			IList JSONData = (IList)(MiniJSON.Json.Deserialize(JSONText));
			if (JSONData != null) {
				for (int o = 0; o < JSONData.Count; o++) {
					IDictionary markerFileData = (IDictionary)JSONData [o];
					MenuJsonData menuJsonData = new MenuJsonData ();
					menuJsonData.sceneName = markerFileData ["Scene"].ToString ();
					float.TryParse(markerFileData ["Version"].ToString (), out menuJsonData.versionForMenuJson);
					menuJsonData.pictureName = markerFileData ["Picture"].ToString ();
					int.TryParse(markerFileData ["PictureWidth"].ToString () ,out menuJsonData.pictureWidth);
					int.TryParse(markerFileData ["PictureHeight"].ToString () ,out menuJsonData.pictureHeight);
					bool.TryParse(markerFileData ["Available"].ToString (), out menuJsonData.available);
					menuJsonData.jsonFile = markerFileData ["JsonFile"].ToString ();
					OnlineMenuJsonDataReference.Add (menuJsonData);
		
				}
				isOnlineMenuJsonLoaded = true;
				Debug.Log ("----Finish load Online Menu Json Successfull---");
				ListCompareFunctionForMenuJsonData ();
			}
		}

	}




	/// <summary>
	/// Reads the json for language.
	///  get version , language name and font package name
	/// </summary>
	/// <param name="metadataPathForMarkerJson">Metadata path for marker json.</param>
	public void ReadOnlineJsonForLanguage (string jsonText)
	{
		string JSONText = jsonText;

		if (!string.IsNullOrEmpty(JSONText))
		{
		//	Debug.Log (JSONText);
			IList JSONData = (IList)(MiniJSON.Json.Deserialize(JSONText));
			if (JSONData != null) {
				for (int o = 0; o < JSONData.Count; o++) {
					IDictionary markerFileData = (IDictionary)JSONData [o];
					LanguageJsonData languageJsonData = new LanguageJsonData ();
					float.TryParse(markerFileData ["Version"].ToString (), out languageJsonData.versionForLanguageJson);
					languageJsonData.languageName = markerFileData ["Language"].ToString ();
					languageJsonData.fontPackageRegularName = markerFileData ["FontPackageRegular"].ToString ();
					languageJsonData.fontPackageBoldName = markerFileData ["FontPackageBold"].ToString ();
					languageJsonData.fontPackageName = markerFileData ["FontPackageName"].ToString ();
					int.TryParse(markerFileData ["FontSize"].ToString () ,out languageJsonData.fontSize );
					bool.TryParse(markerFileData ["isLocal"].ToString (), out languageJsonData.isLocal);
					OnlineLanguageJsonDataReference.Add (languageJsonData);

				}
				Debug.Log ("----Finish load Online Language Json Successfull---");
				isSuccessfulLoadedOnlineJson = true;
				ListCompareFunctionForLanguageJsonData ();
			}
		
		}

	}

	public void ReadOnlineJsonForMarkerAndObject (string jsonText)
	{
		string JSONText = jsonText;
		Debug.Log (JSONText);

		if (!string.IsNullOrEmpty(JSONText))
		{

			IList JSONData = (IList)(MiniJSON.Json.Deserialize(JSONText));
			if (JSONData != null) {
				for (int o = 0; o < JSONData.Count; o++) {
					IDictionary markerFileData = (IDictionary)JSONData [o];
					foreach (string data in markerFileData.Keys) {
						if (data.Equals ("Markers")) {
							IList markerJSONData = (IList)(markerFileData ["Markers"]);
							for (int i = 0; i < markerJSONData.Count; i++) {
								IDictionary markerDatas = (IDictionary)markerJSONData [i];
								//MarkerJsonDataReference
								MarkerJsonData markerJsonData = new MarkerJsonData ();
								markerJsonData.markerDatName = markerDatas ["MarkerDat"].ToString ();
								markerJsonData.markerXmlName = markerDatas ["MarkerXml"].ToString ();
								bool.TryParse(markerDatas ["VuMarker"].ToString (), out markerJsonData.isVuMarker);
								float.TryParse (markerDatas ["Version"].ToString (), out markerJsonData.versionForMarkerJson);
								OnlineMarkerJsonDataReference.Add (markerJsonData);

							}

						}
						if (data.Equals ("Unicorns")) {
							IList markerJSONData = (IList)(markerFileData ["Unicorns"]);
							for (int i = 0; i < markerJSONData.Count; i++) {
								IDictionary markerDatas = (IDictionary)markerJSONData [i];
								ObjectJsonData objectJsonData = new ObjectJsonData ();
								objectJsonData.objectName = markerDatas ["ObjectName"].ToString ();
								float.TryParse (markerDatas ["Version"].ToString (), out objectJsonData.versionForObjectJson);
								objectJsonData.associatedMarkerName = markerDatas ["MarkerName"].ToString ();
								objectJsonData.ObjectPosition = LoadVector3Data (markerDatas,"Position");
								objectJsonData.ObjectScale = LoadVector3Data (markerDatas,"Scale");
								objectJsonData.ObjectRotation =LoadVector3Data (markerDatas,"Rotation");
								OnlineObjectJsonDataReference.Add (objectJsonData);

							}

						}

						if (data.Equals ("Scene")) {
							IList markerJSONData = (IList)(markerFileData["Scene"]);
							for (int i = 0; i < markerJSONData.Count; i++) {
								IDictionary markerDatas = (IDictionary)markerJSONData[i];
								SceneJsonData 	sceneJsonData = new SceneJsonData();
								sceneJsonData.SceneName = markerDatas ["SceneName"].ToString();
								float.TryParse(markerDatas ["Version"].ToString (), out sceneJsonData.Version);
								OnlineOSceneJsonDataReference.Add (sceneJsonData);

							}

						}

						if (data.Equals ("Audio")) {
							IList markerJSONData = (IList)(markerFileData["Audio"]);
							for (int i = 0; i < markerJSONData.Count; i++) {
								IDictionary markerDatas = (IDictionary)markerJSONData[i];
								AudioData audioJsonData = new AudioData();
								audioJsonData.AudioName = markerDatas ["AudioName"].ToString();
								float.TryParse(markerDatas ["Version"].ToString (), out audioJsonData.Version);
								OnlineAudioJsonDataReference.Add (audioJsonData);

							}

						}


						if (data.Equals ("UI")) {
							IList markerJSONData = (IList)(markerFileData["UI"]);
							for (int i = 0; i < markerJSONData.Count; i++) {
								IDictionary markerDatas = (IDictionary)markerJSONData[i];
								UIData UIJsonData = new UIData();
								UIJsonData.UIName = markerDatas ["UIPackName"].ToString();
								float.TryParse(markerDatas ["Version"].ToString (), out UIJsonData.Version);
								OnlineUIJsonDataReference.Add (UIJsonData);

							}

						}
		
						

					}

				}
				Debug.Log ("--load Online Marker and Object Json Successfull--");
				isSuccessfulLoadedLocalJson = true;
				ListCompareFunctionForAudoJsonData ();
				ListCompareFunctionForUIJsonData ();
				ListCompareFunctionForSceneJsonData ();
				ListCompareFunctionForMarkerJsonData ();
				ListCompareFunctionForObjectJsonData ();
				toyCoreDownLoadScript.DownloadMarkerAndObject ();
			}
		}

	}

	#endregion

	#region Load Local Json 
	/* Load Json Section
	 * Load Once Only
	 * Flow :
	 * Get Json From Persistent path For ListOfSceneAndUI.json and LanguageList.json
	 * Read  ListOfSceneAndUI Json  then execute menu delegate function
	 * Read  LanguageList Json  then execute language delegate function
	 * if already load execute delegate
	 * 
	 * 
	 */
	public bool isLocalJsonExistForMenuAndLanguage ()
	{
		string directionary = Path.Combine (Application.persistentDataPath, menuSpecficFolder);
		string fileForlistOfSceneAndUI = Path.Combine (directionary, listOfSceneAndUI);// number of scene and image
		string fileForlanguageList = Path.Combine (directionary, languageList);//number of language
		bool isMenuJsonExist = File.Exists (fileForlistOfSceneAndUI);
		bool isLanguageJsonExist = File.Exists (fileForlanguageList);
		bool isMenuJsonExistSum = File.Exists (fileForlistOfSceneAndUI+GlobalStaticVariable.CHECKSUM_EXT);
		bool isLanguageJsonExistSum = File.Exists (fileForlanguageList+GlobalStaticVariable.CHECKSUM_EXT);
		if (isLanguageJsonExist && isMenuJsonExist && isMenuJsonExistSum && isLanguageJsonExistSum) {
			return true;
		}
		return false;
	}

	//load all Json From Path
	public void LoadAllLocalJson(bool shouldLoad)
	{

		if (!shouldLoad) {
			string directionary = Path.Combine (Application.persistentDataPath, menuSpecficFolder);

			string fileForlistOfSceneAndUI = Path.Combine (directionary, listOfSceneAndUI);// number of scene and image
			string fileForlanguageList = Path.Combine (directionary, languageList);//number of language
			//string fileFormarkerAndObjectList = Path.Combine (directionary, markerAndObjectList);// marker and  object

			Debug.Log ("Path 1 for Menu Scene number and picture :" + fileForlistOfSceneAndUI);
			ReadLocalJsonForMenu (fileForlistOfSceneAndUI);
			Debug.Log ("Path 2 for Menu number language option :" + fileForlanguageList);
			ReadLocalJsonForLanguage (fileForlanguageList);

		} else {
			Debug.Log ("---Reload All Json---");
			DebugMenuJsonDataReference ();
			LocalMarkerJsonDataReference  = new List<MarkerJsonData>();
			LocalObjectJsonDataReference  = new List<ObjectJsonData>();
			LocalSceneJsonDataReference  = new List<SceneJsonData>();
			LocalAudioJsonDataReference  = new List<AudioData>();
			LocalUIJsonDataReference  = new List<UIData>();

			OnlineMarkerJsonDataReference  = new List<MarkerJsonData>();
			OnlineObjectJsonDataReference  = new List<ObjectJsonData>();
			OnlineOSceneJsonDataReference  = new List<SceneJsonData>();
			OnlineAudioJsonDataReference  = new List<AudioData>();
			OnlineUIJsonDataReference  = new List<UIData>();
			CCAssetBundleManager.unLoadAllBundle ();
			if (LocalMenuJsonDataReference.Count > 0 && LocalLanguageJsonDataReference.Count > 0) {
				if (menuUIfunction != null) {
					menuUIfunction ();
				}
				if (languageFunction != null) {
					languageFunction ();
				}
			} else {
				Debug.Log ("Nothing in MenuJsonDataReference List and LanguageJsonDataReference List");
				//LoadAllLocalJson (false);

			}
		}



	}




	//read for menu get number of scene ,scene name, each json file for each scene , picture and custom dimension
	private void ReadLocalJsonForMenu (string metadataPathForMarkerJson)
	{
		string JSONText = File.ReadAllText(metadataPathForMarkerJson);
		Debug.Log (JSONText);

		if (!string.IsNullOrEmpty(JSONText))
		{

			IList JSONData = (IList)(MiniJSON.Json.Deserialize(JSONText));
			if (JSONData != null) {
				for (int o = 0; o < JSONData.Count; o++) {
					IDictionary markerFileData = (IDictionary)JSONData [o];
					MenuJsonData menuJsonData = new MenuJsonData ();
					menuJsonData.sceneName = markerFileData ["Scene"].ToString ();
					float.TryParse(markerFileData ["Version"].ToString (), out menuJsonData.versionForMenuJson);
					menuJsonData.pictureName = markerFileData ["Picture"].ToString ();
					int.TryParse(markerFileData ["PictureWidth"].ToString () ,out menuJsonData.pictureWidth);
					int.TryParse(markerFileData ["PictureHeight"].ToString () ,out menuJsonData.pictureHeight);
					bool.TryParse(markerFileData ["Available"].ToString (), out menuJsonData.available);
					menuJsonData.jsonFile = markerFileData ["JsonFile"].ToString ();
					LocalMenuJsonDataReference.Add (menuJsonData);
	
		
				}

				isLocalMenuJsonLoaded = true;
				Debug.Log ("----Finish load Local Menu Json Successfull---");
				/*if (menuUIfunction != null) {
					menuUIfunction ();
				}*/
			}
		}

	}

	/// <summary>
	/// Reads the json for language.
	///  get version , language name and font package name
	/// </summary>
	/// <param name="metadataPathForMarkerJson">Metadata path for marker json.</param>
	private void ReadLocalJsonForLanguage (string metadataPathForMarkerJson)
	{
		string JSONText = File.ReadAllText(metadataPathForMarkerJson);
		Debug.Log (JSONText);

		if (!string.IsNullOrEmpty(JSONText))
		{

			IList JSONData = (IList)(MiniJSON.Json.Deserialize(JSONText));
			if (JSONData != null) {
				for (int o = 0; o < JSONData.Count; o++) {
					IDictionary markerFileData = (IDictionary)JSONData [o];
					LanguageJsonData languageJsonData = new LanguageJsonData ();
					float.TryParse(markerFileData ["Version"].ToString (), out languageJsonData.versionForLanguageJson);
					languageJsonData.languageName = markerFileData ["Language"].ToString ();
					languageJsonData.fontPackageRegularName = markerFileData ["FontPackageRegular"].ToString ();
					languageJsonData.fontPackageBoldName = markerFileData ["FontPackageBold"].ToString ();
					languageJsonData.fontPackageName = markerFileData ["FontPackageName"].ToString ();
					int.TryParse(markerFileData ["FontSize"].ToString () ,out languageJsonData.fontSize );
					bool.TryParse(markerFileData ["isLocal"].ToString (), out languageJsonData.isLocal);
					LocalLanguageJsonDataReference.Add (languageJsonData);
	
				}

				isLocalLanguageJsonLoaded = true;
				Debug.Log ("----Finish load Local Language Json Successfull---");
				/*if(languageFunction != null){
					languageFunction();
				}*/
			}
			if (isLocalLanguageJsonLoaded && isLocalMenuJsonLoaded) {
				isSuccessfulLoadedLocalJson = true;
			}
		}

	}


	public void LoadSceneMarkerAndObjectJson ()
	{
		string directionary = Path.Combine (Application.persistentDataPath, menuSpecficFolder);
		string jsonName = "";
		foreach(MenuJsonData data in LocalMenuJsonDataReference){
			if (data.sceneName.Equals (GlobalStaticVariable.CurrentOnClickSceneName)) {
				jsonName = data.jsonFile;

			}

		}
		if (string.IsNullOrEmpty(jsonName)) {

			Debug.Log (jsonName + " jsonName is null ");
		}
		string  markerAndObjectListPath = /*markerAndObjectList + */jsonName +GlobalStaticVariable.JSON_EXT;
		string fileFormarkerAndObjectList = Path.Combine (directionary, markerAndObjectListPath);// marker and  object
		Debug.Log( " ---JsonFile-- "+" : "+markerAndObjectListPath  + " ---LocalPath-- "+" : "+fileFormarkerAndObjectList);
		if (File.Exists (fileFormarkerAndObjectList) && File.Exists (fileFormarkerAndObjectList+GlobalStaticVariable.CHECKSUM_EXT)) {
			ReadLocalJsonForMarkerAndObject (fileFormarkerAndObjectList,markerAndObjectListPath);
		} 

			toyCoreDownLoadScript.DownloadObjectAndMarkerJson (markerAndObjectListPath);

	}


	private void ReadLocalJsonForMarkerAndObject (string metadataPathForMarkerJson ,string markerAndObjectListPath)
	{
		string JSONText = File.ReadAllText(metadataPathForMarkerJson);
		Debug.Log (JSONText);

		if (!string.IsNullOrEmpty(JSONText))
		{

			IList JSONData = (IList)(MiniJSON.Json.Deserialize(JSONText));
			for (int o = 0; o < JSONData.Count; o++)
			{
				IDictionary markerFileData = (IDictionary)JSONData[o];
				foreach(string data in markerFileData.Keys){
					if (data.Equals ("Markers")) {
						IList markerJSONData = (IList)(markerFileData["Markers"]);
						for (int i = 0; i < markerJSONData.Count; i++) {
							IDictionary markerDatas = (IDictionary)markerJSONData[i];
							//MarkerJsonDataReference
							MarkerJsonData 	markerJsonData = new MarkerJsonData();
							markerJsonData.markerDatName = markerDatas ["MarkerDat"].ToString();
							markerJsonData.markerXmlName = markerDatas ["MarkerXml"].ToString();
							bool.TryParse(markerDatas ["VuMarker"].ToString (), out markerJsonData.isVuMarker);
							float.TryParse(markerDatas ["Version"].ToString (), out markerJsonData.versionForMarkerJson);
							LocalMarkerJsonDataReference.Add (markerJsonData);
						
						}

					}
					if (data.Equals ("Unicorns")) {
						IList markerJSONData = (IList)(markerFileData["Unicorns"]);
						for (int i = 0; i < markerJSONData.Count; i++) {
							IDictionary markerDatas = (IDictionary)markerJSONData[i];
							ObjectJsonData 	objectJsonData = new ObjectJsonData();
							objectJsonData.objectName = markerDatas ["ObjectName"].ToString();
							float.TryParse(markerDatas ["Version"].ToString (), out objectJsonData.versionForObjectJson);
							objectJsonData.associatedMarkerName = markerDatas ["MarkerName"].ToString ();
							objectJsonData.ObjectPosition = LoadVector3Data (markerDatas,"Position");
							objectJsonData.ObjectScale = LoadVector3Data (markerDatas,"Scale");
							objectJsonData.ObjectRotation =LoadVector3Data (markerDatas,"Rotation");
							LocalObjectJsonDataReference.Add (objectJsonData);
		
						}

					}
					if (data.Equals ("Scene")) {
						IList markerJSONData = (IList)(markerFileData["Scene"]);
						for (int i = 0; i < markerJSONData.Count; i++) {
							IDictionary markerDatas = (IDictionary)markerJSONData[i];
							SceneJsonData 	sceneJsonData = new SceneJsonData();
							sceneJsonData.SceneName = markerDatas ["SceneName"].ToString();
							float.TryParse(markerDatas ["Version"].ToString (), out sceneJsonData.Version);
							LocalSceneJsonDataReference.Add (sceneJsonData);

						}

					}


					if (data.Equals ("Audio")) {
						IList markerJSONData = (IList)(markerFileData ["Audio"]);
						for (int i = 0; i < markerJSONData.Count; i++) {
							IDictionary markerDatas = (IDictionary)markerJSONData [i];
							AudioData audioJsonData = new AudioData ();
							audioJsonData.AudioName = markerDatas ["AudioName"].ToString ();
							float.TryParse (markerDatas ["Version"].ToString (), out audioJsonData.Version);
							LocalAudioJsonDataReference.Add (audioJsonData);

						}
					}

					if (data.Equals ("UI")) {
						IList markerJSONData = (IList)(markerFileData ["UI"]);
						for (int i = 0; i < markerJSONData.Count; i++) {
							IDictionary markerDatas = (IDictionary)markerJSONData [i];
							UIData UIJsonData = new UIData ();
							UIJsonData.UIName = markerDatas ["UIPackName"].ToString ();
							float.TryParse (markerDatas ["Version"].ToString (), out UIJsonData.Version);
							LocalUIJsonDataReference.Add (UIJsonData);

						}
					}

				}
			//	toyCoreDownLoadScript.DownloadObjectAndMarkerJson (markerAndObjectListPath);

			}
			Debug.Log ("----Finish load Local Marker and Object Json Successfull---");
		}

	}

	/// <summary>
	/// Debugs the menu json data reference.
	/// </summary>
	private void DebugMenuJsonDataReference ()
	{
		Debug.Log ("Log MenuJsonDataReference count  :" + LocalMenuJsonDataReference.Count);
		foreach(MenuJsonData data in LocalMenuJsonDataReference ){
			Debug.Log ("Scene Name : " + data.sceneName + " Version : " +data.versionForMenuJson + " PictureName : " + data.pictureName + " Picture width : " + data.pictureWidth + " PictureHeight : " +  data.pictureHeight 
				+ "JsonFile : " + data.jsonFile + " Available : " + data.available);
	
		}
		Debug.Log ("Log LanguageJsonDataReference count  :" + LocalLanguageJsonDataReference.Count);
		foreach(LanguageJsonData data in LocalLanguageJsonDataReference ){
			Debug.Log (" Language Name : " + data.languageName + " Versionn : " + data.versionForLanguageJson + " fontPackageRegularName : " + data.fontPackageRegularName+ " fontPackageBoldName : " + data.fontPackageBoldName);
		}
	}


	#endregion


	#region Compare Local And Online Json


	public void JsonComparsionForOnlineAndLocal()
	{
		ListCompareFunctionForMenuJsonData ();
		ListCompareFunctionForLanguageJsonData ();
		ListCompareFunctionForMarkerJsonData ();
		ListCompareFunctionForMarkerJsonData ();
		DebugMenuJsonDataReference ();
	}

	public void ListCompareFunctionForMenuJsonData ()
	{
		Debug.Log ("=== Compare Menu item ===");
		foreach (MenuJsonData OnlineData in OnlineMenuJsonDataReference){
			string sceneName = OnlineData.sceneName;
			bool isFind = false;
			foreach (MenuJsonData LocalData in LocalMenuJsonDataReference) {
				if (LocalData.sceneName.Equals (sceneName)) {
					isFind = true;
					if (LocalData.versionForMenuJson < OnlineData.versionForMenuJson) {
						LocalData.sceneName = OnlineData.sceneName;
						LocalData.pictureName = OnlineData.pictureName;
						LocalData.pictureWidth = OnlineData.pictureWidth;
						LocalData.pictureHeight = OnlineData.pictureHeight;
						LocalData.versionForMenuJson = OnlineData.versionForMenuJson;
						LocalData.jsonFile = OnlineData.jsonFile;
						LocalData.isDownload = true;
						Debug.Log (OnlineData.sceneName + " Menu item require download");
					} else {
						LocalData.isDownload = false;
						Debug.Log (OnlineData.sceneName + " Menu item  not require download");
					}
				}
			}
				if(!isFind){
					MenuJsonData menuJsonData = new MenuJsonData ();
				Debug.Log (OnlineData.sceneName + " : " + OnlineData.pictureName);
					menuJsonData.sceneName = OnlineData.sceneName;
					menuJsonData.pictureName = OnlineData.pictureName;
					menuJsonData.pictureWidth = OnlineData.pictureWidth;
					menuJsonData.pictureHeight = OnlineData.pictureHeight;
					menuJsonData.versionForMenuJson = OnlineData.versionForMenuJson;
					menuJsonData.jsonFile = OnlineData.jsonFile;
					menuJsonData.isDownload = true;
					LocalMenuJsonDataReference.Add (menuJsonData);
					Debug.Log ("local Menu json not find " + OnlineData.sceneName);
				}
			
		}
		Debug.Log ("======== step 1 === finish Load Menu Json =============");
		toyCoreDownLoadScript.DownloadPicturePNG ();
	
	}

	public void ListCompareFunctionForLanguageJsonData ()
	{
		Debug.Log ("=== Compare Language item ===");
		foreach (LanguageJsonData OnlineData in OnlineLanguageJsonDataReference){
			string languageName = OnlineData.languageName;
			bool isFind = false;
			foreach (LanguageJsonData LocalData in LocalLanguageJsonDataReference) {
				if (LocalData.languageName.Equals (languageName)) {
		
					isFind = true;
					if (LocalData.versionForLanguageJson < OnlineData.versionForLanguageJson) {
						LocalData.languageName = OnlineData.languageName;
						LocalData.fontPackageBoldName = OnlineData.fontPackageBoldName;
						LocalData.fontPackageRegularName = OnlineData.fontPackageRegularName;
						LocalData.versionForLanguageJson = OnlineData.versionForLanguageJson;
						LocalData.fontPackageName = OnlineData.fontPackageName;
						LocalData.fontSize = OnlineData.fontSize;
						LocalData.isLocal = OnlineData.isLocal;
						LocalData.isDownload = true;
						Debug.Log (OnlineData.languageName + " Language require download");
					} else {
						LocalData.isDownload = false;
						Debug.Log (OnlineData.languageName + " Language not require download");
					}
				}
			}
				if(!isFind){
					Debug.Log (OnlineData.languageName);
					LanguageJsonData languageJsonData = new LanguageJsonData ();
					languageJsonData.languageName = OnlineData.languageName;
					languageJsonData.fontPackageBoldName = OnlineData.fontPackageBoldName;
					languageJsonData.fontPackageRegularName = OnlineData.fontPackageRegularName;
					languageJsonData.fontPackageName = OnlineData.fontPackageName;
					languageJsonData.versionForLanguageJson = OnlineData.versionForLanguageJson;
					languageJsonData.fontSize = OnlineData.fontSize;
					languageJsonData.isLocal = OnlineData.isLocal;
					languageJsonData.isDownload = true;
				//bool.TryParse(OnlineData.isDownload, out languageJsonData.isDownload);
					LocalLanguageJsonDataReference.Add (languageJsonData);
					Debug.Log ("local language json not find " + OnlineData.languageName);
				}

		}
	}

	public void ListCompareFunctionForMarkerJsonData ()
	{
		Debug.Log ("=== Compare Marker item ===");
		foreach (MarkerJsonData OnlineData in OnlineMarkerJsonDataReference){
			string markerDatName = OnlineData.markerDatName;
			bool isFind = false;
			foreach (MarkerJsonData LocalData in LocalMarkerJsonDataReference) {
				if (LocalData.markerDatName.Equals (markerDatName)) {
					isFind = true;
					if (LocalData.versionForMarkerJson < OnlineData.versionForMarkerJson) {
						LocalData.markerDatName = OnlineData.markerDatName;
						LocalData.markerXmlName = OnlineData.markerXmlName;
						LocalData.versionForMarkerJson = OnlineData.versionForMarkerJson;
						LocalData.isVuMarker = OnlineData.isVuMarker;
						LocalData.isDownload = true;
						Debug.Log (OnlineData.markerDatName + " marker require download");
					} else {
						LocalData.isDownload = false;
						Debug.Log (OnlineData.markerDatName + " marker not require download");
					}
				}
			}
				if(!isFind){
					MarkerJsonData markerJsonData = new MarkerJsonData ();
					markerJsonData.markerDatName = OnlineData.markerDatName;
					markerJsonData.markerXmlName = OnlineData.markerXmlName;
					markerJsonData.versionForMarkerJson = OnlineData.versionForMarkerJson;
					markerJsonData.isDownload = true;
					markerJsonData.isVuMarker = OnlineData.isVuMarker;

					LocalMarkerJsonDataReference.Add (markerJsonData);
				Debug.Log ("local marker json not find " + OnlineData.markerDatName);
			}
			
			Debug.Log ("end : " +  OnlineMarkerJsonDataReference.Count + " : " + LocalMarkerJsonDataReference.Count);
		}
	}


	public void ListCompareFunctionForObjectJsonData ()
	{
		Debug.Log ("=== Compare Object item ===");
		foreach (ObjectJsonData OnlineData in OnlineObjectJsonDataReference){
			string objectName = OnlineData.objectName;
			bool isFind = false;
			foreach (ObjectJsonData LocalData in LocalObjectJsonDataReference) {
				if (LocalData.objectName.Equals (objectName)) {
					isFind = true;
					if (LocalData.versionForObjectJson < OnlineData.versionForObjectJson) {
						LocalData.objectName = OnlineData.objectName;
						LocalData.versionForObjectJson = OnlineData.versionForObjectJson;
						LocalData.isDownload = true;
						LocalData.associatedMarkerName = OnlineData.associatedMarkerName;
						LocalData.ObjectPosition = OnlineData.ObjectPosition;
						LocalData.ObjectScale = OnlineData.ObjectScale;
						LocalData.ObjectRotation = OnlineData.ObjectRotation;
						Debug.Log (OnlineData.objectName + " object require download");
					} else {
						LocalData.isDownload = false;
						Debug.Log (OnlineData.objectName + " object not require download");
					}
				}
			}
				if(!isFind){
					ObjectJsonData objectJsonData = new ObjectJsonData ();
					objectJsonData.objectName = OnlineData.objectName;
					objectJsonData.versionForObjectJson = OnlineData.versionForObjectJson;
					objectJsonData.isDownload = true;
					LocalObjectJsonDataReference.Add (objectJsonData);
				Debug.Log ("local object json not find " + OnlineData.objectName);
			}
			
		}

	}

	public void ListCompareFunctionForSceneJsonData()
	{
		Debug.Log ("=== Compare Scene item ===");
		foreach (SceneJsonData OnlineData in OnlineOSceneJsonDataReference){
			string SceneName = OnlineData.SceneName;
			bool isFind = false;
			foreach (SceneJsonData LocalData in LocalSceneJsonDataReference) {
				if (LocalData.SceneName.Equals (SceneName)) {
					isFind = true;
					if (LocalData.Version < OnlineData.Version) {
						LocalData.SceneName = OnlineData.SceneName;
						LocalData.Version = OnlineData.Version;
						LocalData.isDownload = true;
						Debug.Log (OnlineData.SceneName + " scene require download");
					} else {
						LocalData.isDownload = false;
						Debug.Log (OnlineData.SceneName + " scene not require download");
					}
				}
			}
			if(!isFind){
				SceneJsonData sceneJsonData = new SceneJsonData ();
				sceneJsonData.SceneName = OnlineData.SceneName;
				sceneJsonData.Version = OnlineData.Version;
				sceneJsonData.isDownload = true;
				LocalSceneJsonDataReference.Add (sceneJsonData);
				Debug.Log ("local object json not find " + OnlineData.SceneName);
			}

		}

	}

	public void ListCompareFunctionForAudoJsonData()
	{
		Debug.Log ("=== Compare Audio item ===");
		foreach (AudioData OnlineData in OnlineAudioJsonDataReference){
			string SceneName = OnlineData.AudioName;
			bool isFind = false;
			foreach (AudioData LocalData in LocalAudioJsonDataReference) {
				if (LocalData.AudioName.Equals (SceneName)) {
					isFind = true;
					if (LocalData.Version < OnlineData.Version) {
						LocalData.AudioName = OnlineData.AudioName;
						LocalData.Version = OnlineData.Version;
						LocalData.isDownload = true;
						Debug.Log (OnlineData.AudioName + " scene require download");
					} else {
						LocalData.isDownload = false;
						Debug.Log (OnlineData.AudioName + " scene not require download");
					}
				}
			}
			if(!isFind){
				AudioData audioJsonData = new AudioData ();
				audioJsonData.AudioName = OnlineData.AudioName;
				audioJsonData.Version = OnlineData.Version;
				audioJsonData.isDownload = true;
				LocalAudioJsonDataReference.Add (audioJsonData);
				Debug.Log ("local object json not find " + OnlineData.AudioName);
			}

		}

	}

	public void ListCompareFunctionForUIJsonData()
	{
		Debug.Log ("=== Compare UI item ===");
		foreach (UIData OnlineData in OnlineUIJsonDataReference){
			string UIPackageName = OnlineData.UIName;
			bool isFind = false;
			foreach (UIData LocalData in LocalUIJsonDataReference) {
				if (LocalData.UIName.Equals (UIPackageName)) {
					isFind = true;
					if (LocalData.Version < OnlineData.Version) {
						LocalData.UIName = OnlineData.UIName;
						LocalData.Version = OnlineData.Version;
						LocalData.isDownload = true;
						Debug.Log (OnlineData.UIName + " scene require download");
					} else {
						LocalData.isDownload = false;
						Debug.Log (OnlineData.UIName + " scene not require download");
					}
				}
			}
			if(!isFind){
				UIData UIJsonData = new UIData ();
				UIJsonData.UIName = OnlineData.UIName;
				UIJsonData.Version = OnlineData.Version;
				UIJsonData.isDownload = true;
				LocalUIJsonDataReference.Add (UIJsonData);
				Debug.Log ("local UI json not find " + OnlineData.UIName);
			}

		}

	}


	#endregion

	public int ReturnNumberOfDownLoadCountForLanguage (){
		int index = 0;
		foreach (LanguageJsonData data in LoadJson.LocalLanguageJsonDataReference) {
			if(data.isDownload){
				index++;
			}
		}
		return index;
	}

	private Vector3 LoadVector3Data (IDictionary data, string field)
	{
		Vector3 returnVec = new Vector3 ();

		if (data.Contains (field)) {
			returnVec.x = System.Convert.ToSingle (((IList)data [field]) [0]);
			returnVec.y = System.Convert.ToSingle (((IList)data [field]) [1]);
			returnVec.z = System.Convert.ToSingle (((IList)data [field]) [2]);
		}

		return returnVec;
	}


}

[System.Serializable]
public class MenuJsonData {

	public string sceneName;
	public string pictureName;
	public int pictureWidth;
	public int pictureHeight;
	public bool available;
	public float versionForMenuJson;
	public string jsonFile;
	public bool isDownload =true;
	public string texturePath;
	public string textureFileName;

}

[System.Serializable]
public class LanguageJsonData {

	public string languageName;
	public string fontPackageRegularName;
	public string fontPackageBoldName;
	public string fontPackageName;
	public float versionForLanguageJson;
	public int fontSize;
	public bool isDownload =true;
	public string texturePath;
	public string textureFileName;
	public bool isLocal;

}

[System.Serializable]
public class MarkerJsonData {

	public string markerDatName;
	public string markerXmlName;
	public bool isVuMarker ;
	public float versionForMarkerJson;
	public bool isDownload =true;
	public string MarkerXmlPath;
	public string MarkerXmlFileName;
	public string MarkerDatPath;
	public string MarkerDatFileName;



}
[System.Serializable]
public class ObjectJsonData {

	public string associatedMarkerName;
	public string objectName;
	public float versionForObjectJson;
	public bool isDownload =true;
	public Vector3 ObjectPosition;
	public Vector3 ObjectScale;
	public Vector3 ObjectRotation;
	public string ObjectPath;
	public string ObjectFileName;

}

[System.Serializable]
public class SceneJsonData {

	public string SceneName;
	public float Version;
	public bool isDownload =true;
	public string ScenePath;
	public string SceneFileName;

}

[System.Serializable]
public class AudioData {

	public string AudioName;
	public float Version;
	public bool isDownload =true;
	public string AudioPath;
	public string AudioFileName;

}

[System.Serializable]
public class UIData {

	public string UIName;
	public float Version;
	public bool isDownload =true;
	public string UIPath;
	public string UIFileName;
}
	

