﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleAndRotation : MonoBehaviour {

	[Range (0.01f, 2f)]
	public float rotateAmount = 1;
	[Range (0.01f, 2f)]
	public float maxTimeForClickHold =0.1f;

	private float lastPosX = 0;
	private float ROTATE_SPEED = 0.2f;
	private float FirstPressPosition = 0;
	private bool checkForRotation = false;
	private bool isMultPress = false;
	private float counterTimeForClickHold = 0;
	private bool isMult = false;


	/// <summary>
	/// Raycasts the hit.
	/// play animation if mouse up 
	/// check is it Hold or fast mouse click
	/// </summary>
	public  void RaycastHit ()
	{
		int numberOfTouch = Input.touchCount;
		bool isMouseClick = Input.GetMouseButtonDown (0);
		bool isMouseUp = Input.GetMouseButtonUp (0);
		bool mouseHold = IsMouseHold();
		isMult = isMultTouch (numberOfTouch);
		if (isMouseClick) {
			RaycastHit hit;
			if (Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit)) {
				GlobalStaticVariable.RayhitObject = hit.collider.gameObject;
			//	Debug.Log ("ray cast hit : " + GlobalStaticVariable.RayhitObject.name);

			} else {
				//GlobalStaticVariable.RayhitObject = null;
			}
		}

		if (isMouseUp  && !isMult ) {
			PlayTargetAnimation (mouseHold);
			GlobalStaticVariable.RayhitObject = null;

		}

	}



	/// <summary>
	/// Plaies the target animation.
	/// </summary>
	/// <param name="mouseHold">If set to <c>true</c> mouse hold.</param>
	public void PlayTargetAnimation (bool mouseHold)
	{
		if (GlobalStaticVariable.RayhitObject == null) {

			return;
		}
		//Debug.Log (CoreController.isDoubleClick);

		if (!mouseHold) {
			AnimatorCoreController animatorCoreController = GlobalStaticVariable.RayhitObject.GetComponent<AnimatorCoreController> ();
			Animator thisAnimator = animatorCoreController.animation;
			if (thisAnimator != null) {
				//Debug.Log ("Call Animation" + GlobalStaticVariable.RayhitObject);
				animatorCoreController.CallForAnimation (GlobalStaticVariable.RayhitObject.name);
			//	AudioListController.instance.PickARandomSoundAndPlaySound(GlobalStaticVariable.RayhitObject.name);
				//if(GlobalStaticVariable.RayhitObject.GetComponent<>();)
				//PickARandomSoundAndPlaySound();
		
			}
		}
	}


	/// <summary>
	/// check is it mult touch
	/// </summary>
	/// <returns><c>true</c>, if mult touch was ised, <c>false</c> otherwise.</returns>
	/// <param name="number">Number.</param>
	private bool isMultTouch (int number)
	{
		if (number >= 2) {
			
			isMultPress = true;
			return true;

		} else if (number == 1 && isMultPress) {


			return true;

		} else if (number == 1 && !isMultPress) {

			isMultPress = false;
			return false;

		} else if(number == 0 ){

			isMultPress = false;
			return false;

		}
		return false;
	}

	/// <summary>
	/// Rotation target.
	/// </summary>
	public void Rotation()
	{

		if (Input.touchCount == 1 ) {

			if (lastPosX != 0) {
				float x = Input.GetTouch (0).position.x - lastPosX;

				RotateObject (x);
			}
			lastPosX = Input.GetTouch (0).position.x;

		} else if ( Input.GetMouseButton (0) ) {

			if (lastPosX != 0) {
				float x = lastPosX - Input.mousePosition.x;
				RotateObject (x);
			}
			lastPosX = Input.mousePosition.x;

		} 
	}

	public void OnTouchEnd()
	{

		#if UNITY_EDITOR
		if (Input.GetMouseButtonUp(0))
		{
			lastPosX = 0;
		}
		#endif
		#if !UNITY_EDITOR
		if (Input.touchCount == 0)
		{
		lastPosX = 0;
		}
		#endif
	}

	/// <summary>
	/// Determines whether this instance is mouse hold.
	/// </summary>
	/// <returns><c>true</c> if this instance is mouse hold; otherwise, <c>false</c>.</returns>
	private bool IsMouseHold ()
	{

		#if !UNITY_EDITOR
		if (Input.touchCount == 1 && Input.GetMouseButton (0)) {

		if (counterTimeForClickHold < maxTimeForClickHold) {
		counterTimeForClickHold += Time.deltaTime;

		} else {
		return true;
		}
		} else {
		if(counterTimeForClickHold < maxTimeForClickHold){

		counterTimeForClickHold = 0;
		return false;
		} else {
		Debug.Log("Time up");
		counterTimeForClickHold = 0;
		return true;
		}
		}
		#endif
		#if UNITY_EDITOR

		if (Input.GetMouseButton (0)) {

			if (counterTimeForClickHold < maxTimeForClickHold) {
				counterTimeForClickHold += Time.deltaTime;

			} else {
				return true;
			}
		} else {
			if(counterTimeForClickHold < maxTimeForClickHold){
				counterTimeForClickHold = 0;
				return false;
			} else {

				counterTimeForClickHold = 0;
				return true;
			}
		}

		return false;
		#endif
		return false;

	}

	/// <summary>
	/// Rotates the object.
	/// </summary>
	/// <param name="distance">Distance.</param>
	private void RotateObject(float distance)
	{
		if (GlobalStaticVariable.RayhitObject != null && GlobalStaticVariable.RayhitObject.GetComponent<AnimatorCoreController> () != null && GlobalStaticVariable.RayhitObject.GetComponent<ControllerVariable> () != null && !isMult) {
			if (GlobalStaticVariable.RayhitObject.GetComponent<AnimatorCoreController> ().animatorStatus != AnimatorCoreController.AnimationStatus.ACTIVE && GlobalStaticVariable.RayhitObject.GetComponent<ControllerVariable> ().isRotateAllowed) {
				GlobalStaticVariable.RayhitObject.transform.Rotate (new Vector3 (0, 0, -distance * ROTATE_SPEED), Space.Self);
			
			}
		}
	}
}
