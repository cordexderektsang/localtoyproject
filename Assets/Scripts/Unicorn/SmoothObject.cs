﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class SmoothObject : MonoBehaviour {

	private Queue<Quaternion> rotations;
	private Queue<Vector3> positions ;
	private VuforiaBehaviour qcarBehavior;

	public int smoothingFrames = 4;
	public Vector3 smoothedPosition;
	public Quaternion smoothedRotation;
	private VuforiaARController vuforia;


	void Start () {

		rotations = new Queue<Quaternion>(smoothingFrames);
		positions = new Queue<Vector3>(smoothingFrames);
		qcarBehavior = GetComponent<VuforiaBehaviour>();

		vuforia = VuforiaARController.Instance;
		//		qcarBehavior.RegisterVuforiaStartedCallback(OnInitialized);
	//	qcarBehavior.RegisterTrackablesUpdatedCallback(OnTrackablesUpdated);

		//vuforia.RegisterVuforiaStartedCallback(OnInitialized);
		vuforia.RegisterTrackablesUpdatedCallback (OnTrackablesUpdated);

	}

	void OnTrackablesUpdated ()
	{
		if (rotations.Count >= smoothingFrames) {
			rotations.Dequeue();
			positions.Dequeue();
		}

		rotations.Enqueue(transform.rotation);
		positions.Enqueue(transform.position);

		Vector4 avgr = Vector4.zero;
		foreach (Quaternion singleRotation in rotations) {
			Math3d.AverageQuaternion(ref avgr, singleRotation, rotations.Peek(), rotations.Count);
		}

		Vector3 avgp = Vector3.zero;
		foreach (Vector3 singlePosition in positions) {
			avgp += singlePosition;
		}
		avgp /= positions.Count;

		smoothedRotation = new Quaternion(avgr.x, avgr.y, avgr.z, avgr.w);
		smoothedPosition = avgp;
	}



	// Update is called once per frame
	void LateUpdate () {
		transform.rotation = smoothedRotation;
		transform.position = smoothedPosition;
	}



	void OnDestroy() {
		vuforia.UnregisterTrackablesUpdatedCallback (OnTrackablesUpdated);
	}

}
	



