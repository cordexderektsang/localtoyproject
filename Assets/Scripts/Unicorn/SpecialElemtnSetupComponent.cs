﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialElemtnSetupComponent : MonoBehaviour {

		//public Transform markRenderObject;
		//public GameObject transitionEffect;
		//public GameObject specialEffect;
		//public ParticleSystemRenderer transitionEffectRender;
		//public ParticleSystemRenderer specialEffectRender;
		//public EmissionController emissionController;
		public AnimatorCoreController animatorCoreController;
		public 	Animator[] AnimatorComponent;
		public AudioPlayScript  audioPlayScript;
		// Use this for initialization
		void Start () 
		{
			audioPlayScript = GetComponent<AudioPlayScript> ();
			AnimatorComponent = GetComponentsInChildren<Animator>(true);
			animatorCoreController = GetComponent<AnimatorCoreController> ();
	
			if (AnimatorComponent.Length > 0 && animatorCoreController != null) {
				animatorCoreController.AnimationStartUp (AnimatorComponent[0],audioPlayScript);
			}
			

		}



	}


