﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.SceneManagement;
public class SwitchCam : MonoBehaviour {
	public GameObject frontRainbowObject;
	public GameObject backRainbowObject;
	public Animator animationForBack;
	public AnimatorCoreController animatorCoreControllerForRainbow;
	public Animator animationForFront;
	public GameObject directionForFront;
	public GameObject directionForBack;

	void Awake ()
	{
		VuforiaARController.Instance.RegisterVuforiaStartedCallback (RestartVuforia);
	}

	void Start ()
	{
		CameraDevice.CameraDirection currentDir = CameraDevice.Instance.GetCameraDirection();
		Debug.Log ("========"+ currentDir.ToString());

		animatorCoreControllerForRainbow =  backRainbowObject.transform.parent.GetComponent<AnimatorCoreController>();

		Animator [] animationForBackray  = backRainbowObject.GetComponentsInChildren<Animator>(true);
		animationForBack = animationForBackray [0];
	
		Animator []  animationForFrontray  = frontRainbowObject.GetComponentsInChildren<Animator>(true);
		animationForFront = animationForFrontray [0];
		VuforiaARController.Instance.UnregisterVuforiaStartedCallback (RestartVuforia);
		StartCoroutine (RestartVuforiaAgain());

	}
	void RestartCamera() 
	{

		VuforiaARController.Instance.RegisterVuforiaStartedCallback (callBack);
	}


	public void RestartVuforia ()
	{
		CameraDevice.Instance.Stop();
		CameraDevice.Instance.Deinit();
		TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
		GameObject.FindObjectOfType<VuforiaBehaviour>().enabled = false;
		VuforiaBehaviour.Instance.enabled = false;
		VuforiaARController.Instance.SetWorldCenterMode(VuforiaARController.WorldCenterMode.CAMERA);
		GameObject.FindObjectOfType<VuforiaBehaviour>().enabled = true;
		VuforiaBehaviour.Instance.enabled = true;
		CameraDevice.Instance.Start();
		TrackerManager.Instance.GetTracker<ObjectTracker>().Start();
	}
	 
	 IEnumerator RestartVuforiaAgain()
	 {

	 	yield return new WaitForEndOfFrame();
	 	RestartVuforia();

	 }


	public void CamSwitch ()
	{
		RestartCamera();
	}

	private void callBack()
	{
		//front no probelm
		CameraDevice.CameraDirection currentDir = CameraDevice.Instance.GetCameraDirection();
		Debug.Log (currentDir.ToString());
		CameraDevice.Instance.Stop();
		CameraDevice.Instance.Deinit();
		TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();

		#if UNITY_IOS

			if (frontRainbowObject.activeInHierarchy) {
				frontRainbowObject.SetActive (false);
				backRainbowObject.SetActive (true);
			//	directionForBack.SetActive(true);
			//	directionForFront.SetActive(false);
				animatorCoreControllerForRainbow.animation = animationForBack;
	
			} else {
				frontRainbowObject.SetActive (true);
				backRainbowObject.SetActive (false);
				//directionForBack.SetActive(false);
			//	directionForFront.SetActive(true);
				animatorCoreControllerForRainbow.animation = animationForFront;
			}
		#endif
		if (currentDir == CameraDevice.CameraDirection.CAMERA_BACK || currentDir == CameraDevice.CameraDirection.CAMERA_DEFAULT) {
			CameraDevice.Instance.Init (CameraDevice.CameraDirection.CAMERA_FRONT);
//			frontRainbowObject.SetActive (true);
//			backRainbowObject.SetActive (false);
		} else {
			CameraDevice.Instance.Init (CameraDevice.CameraDirection.CAMERA_BACK);
//			frontRainbowObject.SetActive (false);
//			backRainbowObject.SetActive (true);
		}
		GameObject.FindObjectOfType<VuforiaBehaviour>().enabled = false;
		VuforiaBehaviour.Instance.enabled = false;
		VuforiaARController.Instance.SetWorldCenterMode(VuforiaARController.WorldCenterMode.CAMERA);

		GameObject.FindObjectOfType<VuforiaBehaviour>().enabled = true;
		VuforiaBehaviour.Instance.enabled = true;

		CameraDevice.Instance.Start();
		TrackerManager.Instance.GetTracker<ObjectTracker>().Start();
		RestartVuforia ();
	}




}
